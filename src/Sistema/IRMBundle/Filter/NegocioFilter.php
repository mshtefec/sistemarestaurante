<?php

namespace Sistema\IRMBundle\Filter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Doctrine\ORM\Query\Filter\SQLFilter;
use Sistema\IRMBundle\Filter\negocioInterface;

/**
 * Description of negocioFilter
 *
 * @author rodrigo
 */
class NegocioFilter extends SQLFilter {

    public function addFilterConstraint(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias) {
        if ($targetEntity->reflClass->implementsInterface('Sistema\IRMBundle\Filter\menuInterface')) {
            return sprintf('%s.%s = %s', $targetTableAlias, 'id', $this->getParameter('menu'));
        } else if ($targetEntity->reflClass->implementsInterface('Sistema\IRMBundle\Filter\negocioMenuInterface')) {
            return sprintf('%s.%s = %s', $targetTableAlias, 'menu_id', $this->getParameter('menu'));
        } if ($targetEntity->reflClass->implementsInterface('Sistema\IRMBundle\Filter\userInterface')) {
            return sprintf('%s.%s = %s', $targetTableAlias, 'user_id', $this->getParameter('user'));
        }else if ($targetEntity->reflClass->implementsInterface('Sistema\IRMBundle\Filter\negocioInterface')) {
            return sprintf('%s.%s = %s', $targetTableAlias, 'business_id', $this->getParameter('negocio'));
        } else {
            return '';
        }
    }

}
