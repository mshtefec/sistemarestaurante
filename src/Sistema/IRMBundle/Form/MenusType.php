<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MenusType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class MenusType extends AbstractType {

    private $deleted = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        //deleted
        if ($builder->getData()->getIsDeleted() == 1) {
            $this->deleted = true;
        } else {
            $this->deleted = false;
        }

        $builder
            ->add('name')
            ->add('description')
            ->add('businesses', 'select2', array(
                'class' => 'Sistema\IRMBundle\Entity\Businesses',
                'url' => 'Menus_autocomplete_businesses_by_user',
                'configs' => array(
                    'multiple' => true, //required true or false
                    'width' => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            ->add('deleted', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Deleted',
                    'required' => false,
                    'data' => $this->deleted
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Menus'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_menus';
    }

}
