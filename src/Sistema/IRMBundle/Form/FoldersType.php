<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * FoldersType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class FoldersType extends AbstractType {

    private $deleted = false;
    private $public = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //public
        if ($builder->getData()->getVisible() == 1) {
            $this->public = true;
        } else {
            $this->public = false;
        }
        //deleted
        /*
        if ($builder->getData()->getIsdeleted() == 1) {
            $this->deleted = true;
        } else {
            $this->deleted = false;
        }*/

        $builder
            //->add('uuid')
            ->add('foreignCode')
            ->add('name')
            ->add('description')
            ->add('visible', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Public',
                    'required' => false,
                    'data' => $this->public
                ))
            ->add('allowordering')
            ->add('status', 'choice', array(
                    'required' => true,                    
                    'choices' => array(
                        0 => 'Available',
                        1 => 'Unavailable',                        
                    ),                    
                ))
//            ->add('iconimagetype')
//            ->add('iconimagecolor')
            ->add('customordering')
//            ->add('creationts')
//            ->add('timestamp')
            ->add('isdeleted', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Deleted',
                    'required' => false,
                    //'data' => $this->deleted
                ))
            /*->add('menu', 'select2', array(
                'class' => 'Sistema\IRMBundle\Entity\Menus',
                'url'   => 'Folders_autocomplete_menu',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))*/
            /*->add('image', 'select2', array(
                'class' => 'Sistema\IRMBundle\Entity\EntityImages',
                'url'   => 'Folders_autocomplete_image',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))*/
            /*->add('products', 'select2', array(
                'class' => 'Sistema\IRMBundle\Entity\Products',
                'url'   => 'Folders_autocomplete_products',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))*/
//            ->add('children', 'select2', array(
//                'class' => 'Sistema\IRMBundle\Entity\Folders',
//                'url'   => 'Folders_autocomplete_children',
//                'configs' => array(
//                    'multiple' => true,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                )
//            ))
            ->add('parent', 'select2', array(
                'class' => 'Sistema\IRMBundle\Entity\Folders',
                'url'   => 'Folders_autocomplete_parent',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Folders'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_irmbundle_folders';
    }
}
