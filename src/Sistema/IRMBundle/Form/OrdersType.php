<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\IRMBundle\Form\OrderItemsType;

/**
 * OrdersType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdersType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('uuid')
                ->add('deviceuuid')
                ->add('note')
                ->add('deliverydate', 'select2', array(
                    'label'              => 'Delivery Date',
                    'translation_domain' => 'SistemaIRMBundle',
                    'class'              => 'Sistema\IrmBundle\Entity\DeliveryDates',
                    'url'                => 'Orders_autocomplete_deliverydate',
                    'configs'            => array(
                        'multiple' => true, //required true or false
                        'width'    => 'off',
                    ),
                    'label_attr'         => array(
                        'class'    => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                    'attr'               => array(
                        'class'    => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                ))
                ->add('amount')
                ->add('taxname')
                ->add('taxvalue')
                ->add('guestsquantity')
                ->add('status', 'choice', array(
                    'required' => true,
                    'choices' => array(
                        0 => 'Abierto',
                        1 => 'Requerido',
                        2 => 'Procesando',
                        3 => 'Listo',
                        4 => 'Entregado',
                        5 => 'Cancelado',
                    ),
                ))
                //->add('isdeleted')
                /* ->add('user', 'select2', array(
                  'class' => 'Sistema\UserBundle\Entity\User',
                  'url'   => 'Orders_autocomplete_user',
                  'configs' => array(
                  'multiple' => false,//required true or false
                  'width'    => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                /* ->add('business', 'select2', array(
                  'class' => 'Sistema\IRMBundle\Entity\Businesses',
                  'url'   => 'Orders_autocomplete_business',
                  'configs' => array(
                  'multiple' => false,//required true or false
                  'width'    => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                ->add('table', 'select2', array(
                    'class' => 'Sistema\IRMBundle\Entity\Tables',
                    'url' => 'Orders_autocomplete_table',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    )
                ))
                 ->add('orderItems', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new OrderItemsType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Orders'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_orders';
    }

}
