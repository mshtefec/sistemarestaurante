<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\IRMBundle\Form\OrderItemsType;
use Sistema\IRMBundle\Form\PricesType;
use Sistema\IRMBundle\Form\PriceModifiersType;

/**
 * ProductsType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ProductsType extends AbstractType {

    private $allowOrdening = false;
    private $visible = false;
    private $deleted = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        //allowOrdening
        if ($builder->getData()->getAllowordering() == 1) {
            $this->allowOrdening = true;
        } else {
            $this->allowOrdening = false;
        }//visible
        if ($builder->getData()->getVisible() == 1) {
            $this->visible = true;
        } else {
            $this->visible = false;
        }
        //deleted
        if ($builder->getData()->getIsDeleted() == 1) {
            $this->deleted = true;
        } else {
            $this->deleted = false;
        }

        $builder
                //->add('guid')
                ->add('foreignCode')
                ->add('name')
                ->add('description')
                ->add('code')
                //->add('visible')
                ->add('status', 'choice', array(
                    'required' => true,
                    'choices' => array(
                        0 => 'Available',
                        1 => 'Unavailable',
                    ),
                ))
                //->add('allowordering')
                /* ->add('menu', 'select2', array(
                  'class' => 'Sistema\IRMBundle\Entity\Menus',
                  'url' => 'Products_autocomplete_menu',
                  'configs' => array(
                  'multiple' => false, //required true or false
                  'width' => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                /* ->add('image', 'select2', array(
                  'class' => 'Sistema\IRMBundle\Entity\EntityImages',
                  'url' => 'Products_autocomplete_image',
                  'configs' => array(
                  'multiple' => false, //required true or false
                  'width' => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                ->add('folder', 'select2', array(
                    'class' => 'Sistema\IRMBundle\Entity\Folders',
                    'url' => 'Products_autocomplete_folder',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    ),
                    'required' => true,
                ))
                /* ->add('orderItems', 'collection', array(
                  'label_attr' => array(
                  'class' => 'col-lg-2 col-md-2 col-sm-2',
                  ),
                  'attr' => array(
                  'class' => 'col-lg-4 col-md-4 col-sm-4',
                  ),
                  'label' => false,
                  'type' => new OrderItemsType(),
                  'allow_add' => true,
                  'allow_delete' => true,
                  'required' => true,
                  'prototype' => true,
                  'prototype_name' => '__name__',
                  'by_reference' => false,
                  )) */
                ->add('prices', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new PricesType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
                ->add('modifierPrices', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new PriceModifiersType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
                ->add('allowOrdening', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'AllowOrdening',
                    'required' => false,
                    'data' => $this->allowOrdening
                ))
                ->add('visible', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Visible',
                    'required' => false,
                    'data' => $this->visible
                ))
                ->add('deleted', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Deleted',
                    'required' => false,
                    'data' => $this->deleted
                ))
                ->add('tags')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Products'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_products';
    }

}
