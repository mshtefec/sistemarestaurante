<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * BusinessesFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class BusinessesFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('guid', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('type', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('name', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('description', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('fancyname', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('country', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('state', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('county', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('city', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('postalcode', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('address', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('lat', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('lng', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('contactemail', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('notifemail', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('mainphone', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('supportphone', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('website', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('businessparamsserialized', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('ispublic', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('rating', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('syncprocesstimestamp', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('creationts', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('timestamp', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('isdeleted', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Businesses'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_irmbundle_businessesfiltertype';
    }
}
