<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProductsType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ItemModifierType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('amount')
                ->add('name')
                ->add('quality')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\ItemModifier'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_itemmodifier';
    }

}
