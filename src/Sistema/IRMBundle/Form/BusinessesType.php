<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\IRMBundle\Form\BusinessParamType;

/**
 * BusinessesType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class BusinessesType extends AbstractType {

    private $deleted = false;
    private $public = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        //public
        if ($builder->getData()->getIspublic() == 1) {
            $this->public = true;
        } else {
            $this->public = false;
        }
        //deleted
        if ($builder->getData()->getIsdeleted() == 1) {
            $this->deleted = true;
        } else {
            $this->deleted = false;
        }

        $builder
                //->add('guid')
                ->add('type', 'choice', array(
                    'required' => true,
                    'choices' => array(
                        0 => 'Otro',
                        1 => 'Hotel',
                        2 => 'Restaurante',
                        3 => 'Bar',
                    ),
                ))
                ->add('name')
                ->add('description')
                ->add('fancyname')
                ->add('country')
                ->add('state')
                ->add('city')
                ->add('postalcode')
                ->add('address')
                ->add('lat')
                ->add('lng')
                ->add('contactemail')
                ->add('notifemail')
                ->add('mainphone')
                ->add('supportphone')
                ->add('website')
                ->add('publico', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Public',
                    'required' => false,
                    'data' => $this->public
                ))
                ->add('deleted', 'checkbox', array(
                    'mapped' => false,
                    'label' => 'Is Deleted',
                    'required' => false,
                    'data' => $this->deleted
                ))
                ->add('businessParam', new BusinessParamType(), array(
                    'data_class' => 'Sistema\IRMBundle\Entity\BusinessParam'))
        //->add('businessparamsserialized')
        //->add('syncprocesstimestamp')
        //->add('isdeleted')
        /* ->add('iconImage', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\EntityImages',
          'url' => 'Businesses_autocomplete_iconImage',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          ))
          ->add('bgImage', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\EntityImages',
          'url' => 'Businesses_autocomplete_bgImage',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        /* ->add('menu', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\Menus',
          'url' => 'Businesses_autocomplete_menu',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        /* ->add('tables', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\Tables',
          'url' => 'Businesses_autocomplete_tables',
          'configs' => array(
          'multiple' => true, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          ))
          ->add('orderItems', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\OrderItems',
          'url' => 'Businesses_autocomplete_orderItems',
          'configs' => array(
          'multiple' => true, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Businesses',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_businesses';
    }

}
