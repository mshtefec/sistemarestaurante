<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\IRMBundle\Form\TaxType;

class BusinessParamType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('currencySimbol')
                ->add('applyTax')
                ->add('showTax')
                ->add('catalogNameToShow')
                ->add('colorTheme')
                ->add('catalogLayout')
                ->add('showSplashBusinessDescription')
                ->add('showCategoryIcon')
                ->add('showSplahBusinessIcon')
                ->add('showCategoryDescription')
                ->add('showSubCategoryIcon')
                ->add('showSubCategoryDescription')
                ->add('ordeningOfCategories')
                ->add('ordeningOfSubCategories')
                ->add('orderingOfItems')
                ->add('typeViewOfItemsOfStandardView')
                ->add('rowsOfItemsPerCategory')
                ->add('decimalSeparator')
                ->add('decimalDigits')
                ->add('thousandsSeparator')
                ->add('showPricesPrivateProfile')
                ->add('showPricePublicProfile')
                ->add('workflowForTakingOrdersPublicProfile')
                ->add('workflowForTakingOrdersPrivateProfile')
                ->add('allowTakingOrdersByGuestsPrivateProfile')
                ->add('allowTakingOrdersByGuestPublicProfile')
                ->add('allowEnteringGuestIdentifierPrivateProfile')
                ->add('allowEnteringGuestIdentifierPublicProfile')
                ->add('allowEnteringDeliveryDatePrivateProfile')
                ->add('allowEnteringDeliveryDatePublicProfile')
                ->add('showWhereAreWeShortcutPrivateProfile')
                ->add('showInformationShortcutPrivateProfile')
                ->add('showWhereAreWeShorcutPublicProfile')
                ->add('showInformationShorcutPublicProfile')
                ->add('dateFormat')
                ->add('timeZoneId')
                ->add('ticketShowBusinessIcon')
                ->add('ticketShowBusinessAddress')
                ->add('ticketShowBusinessMainPhone')
                ->add('ticketShowBusinessContactEmail')
                ->add('ticketShowFreeTextHeader')
                ->add('ticketFreeTextHeader')
                ->add('ticcketShowFreeTextFooter')
                ->add('ticketFreeTextFooter')
                ->add('taxs', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new TaxType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\BusinessParam',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_businessparam';
    }

}
