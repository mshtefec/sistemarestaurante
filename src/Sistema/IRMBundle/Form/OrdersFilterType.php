<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * OrdersFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrdersFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('deviceuuid', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('note', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('deliverydate', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('amount', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('taxname', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('taxvalue', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('creationdate', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('guestsquantity', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('status', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('timestamp', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('isdeleted', 'filter_number_range', array(
                'attr'=> array('class'=>'form-control')
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Orders'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_irmbundle_ordersfiltertype';
    }
}
