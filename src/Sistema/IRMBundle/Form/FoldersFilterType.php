<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * FoldersFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class FoldersFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('guid', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('foreignCode', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('name', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('description', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('visible', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('allowordering', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
//            ->add('status', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('iconimagetype', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('iconimagecolor', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('customordering', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('creationts', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('timestamp', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
//            ->add('isdeleted', 'filter_number_range', array(
//                'attr'=> array('class'=>'form-control')
//            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Folders'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_irmbundle_foldersfiltertype';
    }
}
