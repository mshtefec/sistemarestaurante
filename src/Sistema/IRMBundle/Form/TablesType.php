<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * TablesType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class TablesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('uuid')
                ->add('name')
                ->add('description')
                ->add('code')
                ->add('status', 'choice', array(
                    'required' => true,
                    'choices' => array(
                        0 => 'Available',
                        1 => 'Unavailable',
                    ),
                ))
        //->add('isdeleted')
        /* ->add('image', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\EntityImages',
          'url' => 'Tables_autocomplete_image',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        /* ->add('business', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\Businesses',
          'url'   => 'Tables_autocomplete_business',
          'configs' => array(
          'multiple' => false,//required true or false
          'width'    => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\Tables'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_tables';
    }

}
