<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProductsType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class EntityImagesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fileCropped', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPathCropped',
                    'label' => 'Image Cropped',
                        //'show_path' => true
                ))
                ->add('fileSource', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPathSource',
                    'label' => 'Image Source',
                        //'show_path' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\EntityImages'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_entityimages';
    }

}
