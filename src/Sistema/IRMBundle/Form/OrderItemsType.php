<?php

namespace Sistema\IRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\IRMBundle\Form\ItemModifierType;

/**
 * ProductsType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class OrderItemsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('guid')
                //->add('selectedpricename')
                //->add('selectedpriceamount')
                //->add('customizedpriceamount')
                ->add('quantity')
                ->add('tags')
                ->add('note')
                ->add('status', 'choice', array(
                    'required' => true,
                    'choices' => array(
                        0 => 'Available',
                        1 => 'Unavailable',
                    ),
                ))
                ->add('amount')
                ->add('isdeleted')
                ->add('itemModifiers', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new ItemModifierType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'prototype' => true,
                    'prototype_name' => '__nameembed__',
                    'by_reference' => false,
                ))
        /* ->add('business', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\Businesses',
          'url' => 'Products_autocomplete_businesses',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        /* ->add('order', 'select2', array(
          'class' => 'Sistema\IRMBundle\Entity\Orders',
          'url' => 'Products_autocomplete_orders',
          'configs' => array(
          'multiple' => false, //required true or false
          'width' => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\IRMBundle\Entity\OrderItems'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_irmbundle_orderitems';
    }

}
