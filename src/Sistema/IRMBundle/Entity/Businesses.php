<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\IRMBundle\Filter\userInterface;

/**
 * Businesses
 *
 * @ORM\Table(name="businesses")
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\BusinessesRepository")
 */
class Businesses extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=70, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="fancyName", type="string", length=70, nullable=true)
     */
    private $fancyname;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=100, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=100, nullable=true)
     */
    private $county;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=10, nullable=true)
     */
    private $postalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=0, nullable=true)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=0, nullable=true)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="contactEmail", type="string", length=100, nullable=true)
     */
    private $contactemail;

    /**
     * @var string
     *
     * @ORM\Column(name="notifEmail", type="string", length=100, nullable=true)
     */
    private $notifemail;

    /**
     * @var string
     *
     * @ORM\Column(name="mainPhone", type="string", length=50, nullable=true)
     */
    private $mainphone;

    /**
     * @var string
     *
     * @ORM\Column(name="supportPhone", type="string", length=50, nullable=true)
     */
    private $supportphone;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=200, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="businessParamsSerialized", type="string", nullable=true)
     */
    private $businessparamsserialized;

    /**
     * @var integer
     *
     * @ORM\Column(name="isPublic", type="integer", nullable=true)
     */
    private $ispublic;

    /**
     * @var float
     *
     * @ORM\Column(name="rating", type="float", precision=10, scale=0, nullable=true)
     */
    private $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="syncProcessTimeStamp", type="bigint", nullable=false)
     */
    private $syncprocesstimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\EntityImages
     *
     * @ORM\OneToOne(targetEntity="Sistema\IRMBundle\Entity\EntityImages")
     * @ORM\JoinColumn(name="icon_image_id", referencedColumnName="id")
     */
    private $iconImage;

    /**
     * @var \Sistema\IRMBundle\Entity\EntityImages
     *
     * @ORM\OneToOne(targetEntity="Sistema\IRMBundle\Entity\EntityImages")
     * @ORM\JoinColumn(name="bg_image_id", referencedColumnName="id")
     */
    private $bgImage;

    /**
     * @var \Sistema\IRMBundle\Entity\User_businesses
     *
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\UserBusinesses", mappedBy="business",cascade={"all"})
     */
    private $userBusinesses;

    /**
     * @var \Sistema\IRMBundle\Entity\Menus
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Menus",inversedBy="businesses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     */
    private $menu;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Tables", mappedBy="business")
     * */
    private $tables;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\OrderItems", mappedBy="business")
     * */
    private $orderItems;

    /**
     * @ORM\OneToOne(targetEntity="BusinessParam", mappedBy="business", cascade={"all"})
     * */
    private $businessParam;

    /**
     * Constructor
     */
    public function __construct() {
        $this->userBusinesses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tables = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orderItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Businesses
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Businesses
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Businesses
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Businesses
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set fancyname
     *
     * @param string $fancyname
     * @return Businesses
     */
    public function setFancyname($fancyname) {
        $this->fancyname = $fancyname;

        return $this;
    }

    /**
     * Get fancyname
     *
     * @return string 
     */
    public function getFancyname() {
        return $this->fancyname;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Businesses
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Businesses
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set county
     *
     * @param string $county
     * @return Businesses
     */
    public function setCounty($county) {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string 
     */
    public function getCounty() {
        return $this->county;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Businesses
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set postalcode
     *
     * @param string $postalcode
     * @return Businesses
     */
    public function setPostalcode($postalcode) {
        $this->postalcode = $postalcode;

        return $this;
    }

    /**
     * Get postalcode
     *
     * @return string 
     */
    public function getPostalcode() {
        return $this->postalcode;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Businesses
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Businesses
     */
    public function setLat($lat) {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat() {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     * @return Businesses
     */
    public function setLng($lng) {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float 
     */
    public function getLng() {
        return $this->lng;
    }

    /**
     * Set contactemail
     *
     * @param string $contactemail
     * @return Businesses
     */
    public function setContactemail($contactemail) {
        $this->contactemail = $contactemail;

        return $this;
    }

    /**
     * Get contactemail
     *
     * @return string 
     */
    public function getContactemail() {
        return $this->contactemail;
    }

    /**
     * Set notifemail
     *
     * @param string $notifemail
     * @return Businesses
     */
    public function setNotifemail($notifemail) {
        $this->notifemail = $notifemail;

        return $this;
    }

    /**
     * Get notifemail
     *
     * @return string 
     */
    public function getNotifemail() {
        return $this->notifemail;
    }

    /**
     * Set mainphone
     *
     * @param string $mainphone
     * @return Businesses
     */
    public function setMainphone($mainphone) {
        $this->mainphone = $mainphone;

        return $this;
    }

    /**
     * Get mainphone
     *
     * @return string 
     */
    public function getMainphone() {
        return $this->mainphone;
    }

    /**
     * Set supportphone
     *
     * @param string $supportphone
     * @return Businesses
     */
    public function setSupportphone($supportphone) {
        $this->supportphone = $supportphone;

        return $this;
    }

    /**
     * Get supportphone
     *
     * @return string 
     */
    public function getSupportphone() {
        return $this->supportphone;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Businesses
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set businessparamsserialized
     *
     * @param string $businessparamsserialized
     * @return Businesses
     */
    public function setBusinessparamsserialized($businessparamsserialized) {
        $this->businessparamsserialized = $businessparamsserialized;

        return $this;
    }

    /**
     * Get businessparamsserialized
     *
     * @return string 
     */
    public function getBusinessparamsserialized() {
        return $this->businessparamsserialized;
    }

    /**
     * Set ispublic
     *
     * @param integer $ispublic
     * @return Businesses
     */
    public function setIspublic($ispublic) {
        $this->ispublic = $ispublic;

        return $this;
    }

    /**
     * Get ispublic
     *
     * @return integer 
     */
    public function getIspublic() {
        return $this->ispublic;
    }

    /**
     * Set rating
     *
     * @param float $rating
     * @return Businesses
     */
    public function setRating($rating) {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float 
     */
    public function getRating() {
        return $this->rating;
    }

    /**
     * Set syncprocesstimestamp
     *
     * @param integer $syncprocesstimestamp
     * @return Businesses
     */
    public function setSyncprocesstimestamp($syncprocesstimestamp) {
        $this->syncprocesstimestamp = $syncprocesstimestamp;

        return $this;
    }

    /**
     * Get syncprocesstimestamp
     *
     * @return integer 
     */
    public function getSyncprocesstimestamp() {
        return $this->syncprocesstimestamp;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Businesses
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set iconImage
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $iconImage
     * @return Businesses
     */
    public function setIconImage(\Sistema\IRMBundle\Entity\EntityImages $iconImage = null) {
        $this->iconImage = $iconImage;

        return $this;
    }

    /**
     * Get iconImage
     *
     * @return \Sistema\IRMBundle\Entity\EntityImages 
     */
    public function getIconImage() {
        return $this->iconImage;
    }

    /**
     * Set bgImage
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $bgImage
     * @return Businesses
     */
    public function setBgImage(\Sistema\IRMBundle\Entity\EntityImages $bgImage = null) {
        $this->bgImage = $bgImage;

        return $this;
    }

    /**
     * Get bgImage
     *
     * @return \Sistema\IRMBundle\Entity\EntityImages 
     */
    public function getBgImage() {
        return $this->bgImage;
    }

    /**
     * Add userBusinesses
     *
     * @param \Sistema\IRMBundle\Entity\User_businesses $userBusinesses
     * @return Businesses
     */
    public function addUserBusiness(\Sistema\IRMBundle\Entity\UserBusinesses $userBusinesses) {
        $userBusinesses->setBusiness($this);
        $this->userBusinesses[] = $userBusinesses;

        return $this;
    }

    /**
     * Remove userBusinesses
     *
     * @param \Sistema\IRMBundle\Entity\User_businesses $userBusinesses
     */
    public function removeUserBusiness(\Sistema\IRMBundle\Entity\UserBusinesses $userBusinesses) {
        $this->userBusinesses->removeElement($userBusinesses);
    }

    /**
     * Get userBusinesses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserBusinesses() {
        return $this->userBusinesses;
    }

    /**
     * Set menu
     *
     * @param \Sistema\IRMBundle\Entity\Menus $menu
     * @return Businesses
     */
    public function setMenu(\Sistema\IRMBundle\Entity\Menus $menu = null) {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \Sistema\IRMBundle\Entity\Menus 
     */
    public function getMenu() {
        return $this->menu;
    }

    /**
     * Add tables
     *
     * @param \Sistema\IRMBundle\Entity\Tables $tables
     * @return Businesses
     */
    public function addTable(\Sistema\IRMBundle\Entity\Tables $tables) {
        $this->tables[] = $tables;

        return $this;
    }

    /**
     * Remove tables
     *
     * @param \Sistema\IRMBundle\Entity\Tables $tables
     */
    public function removeTable(\Sistema\IRMBundle\Entity\Tables $tables) {
        $this->tables->removeElement($tables);
    }

    /**
     * Get tables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTables() {
        return $this->tables;
    }

    /**
     * Add orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     * @return Businesses
     */
    public function addOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $this->orderItems[] = $orderItems;

        return $this;
    }

    /**
     * Remove orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     */
    public function removeOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $this->orderItems->removeElement($orderItems);
    }

    /**
     * Get orderItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderItems() {
        return $this->orderItems;
    }

    /**
     * Set businessParam
     *
     * @param \Sistema\IRMBundle\Entity\BusinessParam $businessParam
     * @return Businesses
     */
    public function setBusinessParam(\Sistema\IRMBundle\Entity\BusinessParam $businessParam = null) {
        $businessParam->setBusiness($this);
        $this->businessParam = $businessParam;

        return $this;
    }

    /**
     * Get businessParam
     *
     * @return \Sistema\IRMBundle\Entity\BusinessParam 
     */
    public function getBusinessParam() {
        return $this->businessParam;
    }
}
