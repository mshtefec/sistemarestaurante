<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\IRMBundle\Filter\userInterface;
use Rhumsaa\Uuid\Uuid;
/**
 * EntityImages
 *
 * @ORM\Table(name="entity_images")
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\EntityImagesRepository")
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class EntityImages extends MWSgedmo  {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="typesIncluded", type="string", length=30, nullable=true)
     */
    private $typesincluded;

    /**
     * @var integer
     *
     * @ORM\Column(name="syncProcessTimeStamp", type="bigint", nullable=false)
     */
    private $syncprocesstimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="entityImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @Assert\File()
     */
    protected $fileCropped;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path_Cropped", type="string", length=255, nullable=true)
     */
    protected $filePathCropped;

    /**
     * @var string
     */
    protected $tempCropped;

    /**
     * @var string
     */
    protected $uploadDir = 'uploads';

    /**
     * @Assert\File()
     */
    protected $fileSource;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path_Source", type="string", length=255, nullable=true)
     */
    protected $filePathSource;

    /**
     * @var string
     */
    protected $tempSource;

    public function __construct() {
        $this->setIsdeleted(0);
    }

    public function __toString() {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $uuid
     * @return EntityImages
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set typesincluded
     *
     * @param string $typesincluded
     * @return EntityImages
     */
    public function setTypesincluded($typesincluded) {
        $this->typesincluded = $typesincluded;

        return $this;
    }

    /**
     * Get typesincluded
     *
     * @return string 
     */
    public function getTypesincluded() {
        return $this->typesincluded;
    }

    /**
     * Set syncprocesstimestamp
     *
     * @param integer $syncprocesstimestamp
     * @return EntityImages
     */
    public function setSyncprocesstimestamp($syncprocesstimestamp) {
        $this->syncprocesstimestamp = $syncprocesstimestamp;

        return $this;
    }

    /**
     * Get syncprocesstimestamp
     *
     * @return integer 
     */
    public function getSyncprocesstimestamp() {
        return $this->syncprocesstimestamp;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return EntityImages
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\user $user
     * @return EntityImages
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set filePath
     *
     * @param  string $filePath
     * @return File
     */
    public function setFilePathCropped($filePathCropped) {
        $this->filePathCropped = $filePathCropped;

        return $this;
    }

    /**
     * Get FilePathCropped
     *
     * @return string
     */
    public function getFilePathCropped() {
        return $this->filePathCropped;
    }

    /**
     * Set filePath
     *
     * @param  string $filePathCropped
     * @return File
     */
    public function setFilePathSource($filePathSource) {
        $this->filePathSource = $filePathSource;

        return $this;
    }

    /**
     * Get FilePathSource
     *
     * @return string
     */
    public function getFilePathSource() {
        return $this->filePathSource;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     */
    public function setFileCropped($file = null) {
        $this->fileCropped = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath($this->filePathCropped))) {
            // store the old name to delete after the update
            $this->tempCropped = $this->getAbsolutePath($this->filePathCropped);
        }
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFileCropped() {
        return $this->fileCropped;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     */
    public function setFileSource($file = null) {
        $this->fileSource = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath($this->filePathSource))) {
            // store the old name to delete after the update
            $this->tempSource = $this->getAbsolutePath($this->filePathSource);
        }
    }

    /**
     * Get file
     *
     * @return UploadedFile
     */
    public function getFileSource() {
        return $this->fileSource;
    }

    public function getAbsolutePath($filePath) {
        return is_null($filePath) ? null : $this->getUploadRootDir() . '/' . $filePath
        ;
    }

    public function getWebPathCropped() {
        return is_null($this->filePathCropped) ? $this->getUploadDir() . '/../bundles/sistemairm/img/img_default.png' : $this->getUploadDir() . '/' . $this->getUser()->getUuid() . '/' . $this->filePathCropped
        ;
    }

    public function getWebPathSource() {
        return is_null($this->filePathSource) ? $this->getUploadDir() . '/../bundles/sistemairm/img/img_default.png' : $this->getUploadDir() . '/' . $this->getUser()->getUuid() . '/' . $this->filePathSource
        ;
    }

    protected function getUploadRootDir() {
        if (!$this->getUploadDir()) {
            $uploadDir = 'uploads';
        } else {
            $uploadDir = $this->getUploadDir();
        }

        $path = __DIR__ . '/../../../../web/' . $uploadDir;

        if (!file_exists($path)) {
            mkdir($path, 0755);
        }

        return $path;
    }

    public function setUploadDir($uploadDir) {
        $this->uploadDir = $uploadDir;
    }

    public function getUploadDir() {
        return $this->uploadDir;
    }

    /**
     * @ORM\PreFlush()
     */
    public function preUpload() {
        if (!is_null($this->getFileCropped())) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->filePathCropped = $filename . "_cropped.png"; //. '.' // $this->getFileCropped()->guessExtension();
        }
        if (!is_null($this->getFileSource())) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->filePathSource = $filename . "_source.png"; //. '.' //$this->getFileSource()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (!is_null($this->getFileCropped())) {
            // if there is an error when moving the file, an exception will
            // be automatically thrown by move(). This will properly prevent
            // the entity from being persisted to the database on error
            if ($this->getFileCropped()instanceof UploadedFile) {
                $this->getFileCropped()->move($this->getUploadRootDir(), $this->filePathCropped);
            } else {
                file_put_contents($this->getUploadRootDir() . '/' . $this->filePathCropped, $this->getFileCropped());
            }
            // check if we have an old image
            if (isset($this->tempCropped)) {
                if (file_exists($this->tempCropped)) {
                    // delete the old image
                    unlink($this->tempCropped);
                }
                // clear the temp image path
                $this->tempCropped = null;
            }
            $this->fileCropped = null;
        }
        if (is_null($this->getFileSource())) {
            return null;
        } else {

            // if there is an error when moving the file, an exception will
            // be automatically thrown by move(). This will properly prevent
            // the entity from being persisted to the database on error
            if ($this->getFileSource()instanceof UploadedFile) {
                $this->getFileSource()->move($this->getUploadRootDir(), $this->filePathSource);
            } else {
                file_put_contents($this->getUploadRootDir() . '/' . $this->filePathSource, $this->getFileSource());
            }
            // check if we have an old image
            if (isset($this->tempSource)) {
                if (file_exists($this->tempSource)) {
                    // delete the old image
                    unlink($this->tempSource);
                }
                // clear the temp image path
                $this->tempSource = null;
            }
            $this->fileSource = null;
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove() {
        $this->tempCropped = $this->getAbsolutePath($this->filePathCropped);
        $this->tempSource = $this->getAbsolutePath($this->filePathSource);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if (isset($this->tempCropped)) {
            if (file_exists($this->tempCropped)) {
                unlink($this->tempCropped);
            }
        }
        if (isset($this->tempSource)) {
            if (file_exists($this->tempSource)) {
                unlink($this->tempSource);
            }
        }
    }

    public function getFilePath() {
        return $this->getFilePathCropped();
    }

}
