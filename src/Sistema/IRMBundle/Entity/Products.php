<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\IRMBundle\Filter\negocioMenuInterface;

/**
 * Products
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\ProductsRepository")
 * @UniqueEntity({"uuid"})
 */
class Products extends MWSgedmo implements negocioMenuInterface{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="foreign_code", type="string", length=36, nullable=true)
     */
    private $foreignCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="priceArraySerialized", type="string", nullable=true)
     */
    private $pricearrayserialized;

    /**
     * @var string
     *
     * @ORM\Column(name="modifierPriceArraySerialized", type="string", nullable=true)
     */
    private $modifierpricearrayserialized;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="string", length=10, nullable=false)
     */
    private $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="allowOrdering", type="string", length=10, nullable=false)
     */
    private $allowordering;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="iconImageType", type="integer", nullable=true)
     */
    private $iconimagetype;

    /**
     * @var integer
     *
     * @ORM\Column(name="iconImageColor", type="integer", nullable=true)
     */
    private $iconimagecolor;

    /**
     * @var integer
     *
     * @ORM\Column(name="customOrdering", type="integer", nullable=true)
     */
    private $customordering;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\Menus
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Menus", inversedBy="products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     */
    private $menu;

    /**
     * @var \Sistema\IRMBundle\Entity\EntityImages
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\EntityImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var \Sistema\IRMBundle\Entity\Folders
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Folders", inversedBy="products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     * })
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\OrderItems", mappedBy="product",cascade={"all"})
     * */
    private $orderItems;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Prices", mappedBy="product" ,cascade={"all"})
     * */
    private $prices;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\PriceModifiers", mappedBy="product" ,cascade={"all"})
     * */
    private $modifierPrices;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255)
     */
    private $tags;

    public function __toString() {
        return $this->getName();
    }
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->orderItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modifierPrices = new \Doctrine\Common\Collections\ArrayCollection();
        //para probar
        $this->setIconimagetype(1);
        $this->setIconimagecolor(1);
        $this->setCustomordering(1);
        $this->setIsdeleted(0);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $uuid
     * @return Products
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set foreignCode
     *
     * @param string $foreignCode
     * @return Products
     */
    public function setForeignCode($foreignCode) {
        $this->foreignCode = $foreignCode;

        return $this;
    }

    /**
     * Get foreignCode
     *
     * @return string 
     */
    public function getForeignCode() {
        return $this->foreignCode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Products
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Products
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Products
     */
    public function setCode($code) {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * Set pricearrayserialized
     *
     * @param string $pricearrayserialized
     * @return Products
     */
    public function setPricearrayserialized($pricearrayserialized) {
        $this->pricearrayserialized = $pricearrayserialized;

        return $this;
    }

    /**
     * Get pricearrayserialized
     *
     * @return string 
     */
    public function getPricearrayserialized() {
        return $this->pricearrayserialized;
    }

    /**
     * Set modifierpricearrayserialized
     *
     * @param string $modifierpricearrayserialized
     * @return Products
     */
    public function setModifierpricearrayserialized($modifierpricearrayserialized) {
        $this->modifierpricearrayserialized = $modifierpricearrayserialized;

        return $this;
    }

    /**
     * Get modifierpricearrayserialized
     *
     * @return string 
     */
    public function getModifierpricearrayserialized() {
        return $this->modifierpricearrayserialized;
    }

    /**
     * Set visible
     *
     * @param string $visible
     * @return Products
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string 
     */
    public function getVisible() {
        return $this->visible;
    }

    /**
     * Set allowordering
     *
     * @param string $allowordering
     * @return Products
     */
    public function setAllowordering($allowordering) {
        $this->allowordering = $allowordering;

        return $this;
    }

    /**
     * Get allowordering
     *
     * @return string 
     */
    public function getAllowordering() {
        return $this->allowordering;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Products
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set iconimagetype
     *
     * @param integer $iconimagetype
     * @return Products
     */
    public function setIconimagetype($iconimagetype) {
        $this->iconimagetype = $iconimagetype;

        return $this;
    }

    /**
     * Get iconimagetype
     *
     * @return integer 
     */
    public function getIconimagetype() {
        return $this->iconimagetype;
    }

    /**
     * Set iconimagecolor
     *
     * @param integer $iconimagecolor
     * @return Products
     */
    public function setIconimagecolor($iconimagecolor) {
        $this->iconimagecolor = $iconimagecolor;

        return $this;
    }

    /**
     * Get iconimagecolor
     *
     * @return integer 
     */
    public function getIconimagecolor() {
        return $this->iconimagecolor;
    }

    /**
     * Set customordering
     *
     * @param integer $customordering
     * @return Products
     */
    public function setCustomordering($customordering) {
        $this->customordering = $customordering;

        return $this;
    }

    /**
     * Get customordering
     *
     * @return integer 
     */
    public function getCustomordering() {
        return $this->customordering;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Products
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Products
     */
    public function setTags($tags) {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags() {
        return $this->tags;
    }

    /**
     * Set menu
     *
     * @param \Sistema\IRMBundle\Entity\Menus $menu
     * @return Products
     */
    public function setMenu(\Sistema\IRMBundle\Entity\Menus $menu = null) {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \Sistema\IRMBundle\Entity\Menus 
     */
    public function getMenu() {
        return $this->menu;
    }

    /**
     * Set image
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $image
     * @return Products
     */
    public function setImage(\Sistema\IRMBundle\Entity\EntityImages $image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Sistema\IRMBundle\Entity\EntityImages 
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set folder
     *
     * @param \Sistema\IRMBundle\Entity\Folders $folder
     * @return Products
     */
    public function setFolder(\Sistema\IRMBundle\Entity\Folders $folder = null) {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \Sistema\IRMBundle\Entity\Folders 
     */
    public function getFolder() {
        return $this->folder;
    }

    /**
     * Add orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     * @return Products
     */
    public function addOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $orderItems->setProduct($this);
        $this->orderItems[] = $orderItems;

        return $this;
    }

    /**
     * Remove orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     */
    public function removeOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $this->orderItems->removeElement($orderItems);
    }

    /**
     * Get orderItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderItems() {
        return $this->orderItems;
    }

    /**
     * Add prices
     *
     * @param \Sistema\IRMBundle\Entity\Prices $prices
     * @return Products
     */
    public function addPrice(\Sistema\IRMBundle\Entity\Prices $prices) {

        $prices->setProduct($this);
        $this->prices[] = $prices;

        return $this;
    }

    /**
     * Remove prices
     *
     * @param \Sistema\IRMBundle\Entity\Prices $prices
     */
    public function removePrice(\Sistema\IRMBundle\Entity\Prices $prices) {
        $this->prices->removeElement($prices);
    }

    /**
     * Get prices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrices() {
        return $this->prices;
    }

    /**
     * Add modifierPrices
     *
     * @param \Sistema\IRMBundle\Entity\PriceModifiers $modifierPrices
     * @return Products
     */
    public function addModifierPrice(\Sistema\IRMBundle\Entity\PriceModifiers $modifierPrices) {

        $modifierPrices->setProduct($this);
        $this->modifierPrices[] = $modifierPrices;

        return $this;
    }

    /**
     * Remove modifierPrices
     *
     * @param \Sistema\IRMBundle\Entity\PriceModifiers $modifierPrices
     */
    public function removeModifierPrice(\Sistema\IRMBundle\Entity\PriceModifiers $modifierPrices) {
        $this->modifierPrices->removeElement($modifierPrices);
    }

    /**
     * Get modifierPrices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModifierPrices() {
        return $this->modifierPrices;
    }
}
