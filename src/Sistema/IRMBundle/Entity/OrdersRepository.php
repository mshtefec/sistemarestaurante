<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * OrdersRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrdersRepository extends EntityRepository
{
	public function getOrdersByUserId($userId) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('o')
            ->from('Sistema\IRMBundle\Entity\Orders', 'o')
            ->where('o.user = :id')
            ->setParameter('id', $userId)
            ->orderBy('o.id', 'DESC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
}