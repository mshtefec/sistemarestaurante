<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tax
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\TaxRepository")
 */
class Tax {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefault", type="boolean")
     */
    private $isDefault;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessParam", inversedBy="taxs")
     * @ORM\JoinColumn(name="businessParam_id", referencedColumnName="id")
     * */
    private $businessParam;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tax
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return Tax
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     * @return Tax
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean 
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set businessParam
     *
     * @param \Sistema\IRMBundle\Entity\BusinessParam $businessParam
     * @return Tax
     */
    public function setBusinessParam(\Sistema\IRMBundle\Entity\BusinessParam $businessParam = null)
    {
        $this->businessParam = $businessParam;

        return $this;
    }

    /**
     * Get businessParam
     *
     * @return \Sistema\IRMBundle\Entity\BusinessParam 
     */
    public function getBusinessParam()
    {
        return $this->businessParam;
    }
}
