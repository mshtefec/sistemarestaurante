<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemLogs
 *
 * @ORM\Table(name="system_logs")
 * @ORM\Entity
 */
class SystemLogs
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="business_id", type="integer", nullable=false)
     */
    private $businessId;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_id", type="integer", nullable=true)
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="extraData", type="blob", nullable=true)
     */
    private $extradata;

    /**
     * @var string
     *
     * @ORM\Column(name="dataReceived", type="blob", nullable=true)
     */
    private $datareceived;

    /**
     * @var string
     *
     * @ORM\Column(name="dataToSend", type="blob", nullable=true)
     */
    private $datatosend;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeStamp", type="datetime", nullable=true)
     */
    private $timestamp;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessId
     *
     * @param integer $businessId
     * @return SystemLogs
     */
    public function setBusinessId($businessId)
    {
        $this->businessId = $businessId;

        return $this;
    }

    /**
     * Get businessId
     *
     * @return integer 
     */
    public function getBusinessId()
    {
        return $this->businessId;
    }

    /**
     * Set deviceId
     *
     * @param integer $deviceId
     * @return SystemLogs
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return integer 
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SystemLogs
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SystemLogs
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set extradata
     *
     * @param string $extradata
     * @return SystemLogs
     */
    public function setExtradata($extradata)
    {
        $this->extradata = $extradata;

        return $this;
    }

    /**
     * Get extradata
     *
     * @return string 
     */
    public function getExtradata()
    {
        return $this->extradata;
    }

    /**
     * Set datareceived
     *
     * @param string $datareceived
     * @return SystemLogs
     */
    public function setDatareceived($datareceived)
    {
        $this->datareceived = $datareceived;

        return $this;
    }

    /**
     * Get datareceived
     *
     * @return string 
     */
    public function getDatareceived()
    {
        return $this->datareceived;
    }

    /**
     * Set datatosend
     *
     * @param string $datatosend
     * @return SystemLogs
     */
    public function setDatatosend($datatosend)
    {
        $this->datatosend = $datatosend;

        return $this;
    }

    /**
     * Get datatosend
     *
     * @return string 
     */
    public function getDatatosend()
    {
        return $this->datatosend;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return SystemLogs
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
