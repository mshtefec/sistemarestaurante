<?php

namespace Sistema\IRMBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;
use Sistema\IRMBundle\Filter\negocioMenuInterface;

/**
 * Categories
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="categories", uniqueConstraints={@ORM\UniqueConstraint(name="user_menu_uuid", columns={"menu_id", "uuid"})}, indexes={@ORM\Index(name="idx_fe37d30fccd7e912", columns={"menu_id"}), @ORM\Index(name="idx_fe37d30f3da5256d", columns={"image_id"}), @ORM\Index(name="user_menu_isdeleted", columns={"menu_id", "isDeleted"})})
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @UniqueEntity({"uuid"})
 *
 */
class Category extends MWSgedmo implements negocioMenuInterface {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     * @Serializer\Exclude 
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="foreign_code", type="string", length=36, nullable=true)
     * @Serializer\Exclude 
     */
    private $foreignCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false) 
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     * @Serializer\Exclude
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="string", length=10, nullable=false)
     * @Serializer\Exclude 
     */
    private $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="allowOrdering", type="string", length=10, nullable=false)
     * @Serializer\Exclude 
     */
    private $allowordering;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     * @Serializer\Exclude 
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="iconImageType", type="integer", nullable=true)
     * @Serializer\Exclude 
     */
    private $iconimagetype;

    /**
     * @var integer
     *
     * @ORM\Column(name="iconImageColor", type="integer", nullable=true)
     * @Serializer\Exclude 
     */
    private $iconimagecolor;

    /**
     * @var integer
     *
     * @ORM\Column(name="customOrdering", type="integer", nullable=true)
     * @Serializer\Exclude 
     */
    private $customordering;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     * @Serializer\Exclude 
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\Menus
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Menus", inversedBy="folders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude 
     */
    private $menu;

    /**
     * @var \Sistema\IRMBundle\Entity\EntityImages
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\EntityImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude 
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Products", mappedBy="folder")
     * @Serializer\Exclude 
     * */
    private $products;

    /**
     * @var category
     *
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Folders", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id") 
     * @Gedmo\TreeParent
     */
    private $parent;

    /*
     * $rgt, $lft, $lvl, $root Son atributos propios de Gedmo, los utiliza para
     * crear el arbol y se setean automaticamente al definir los parent
     * se pueden utilizar getParent para saber el padre al que pertenece la
     * subcategoria, y getChildren para saber desde un padre su o sus subcategorias
     * siempre trae como resultado un array de elementos folder.  
     */

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * Constructor
     */
    public function __construct() {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Set rgt
     *
     * @param integer $lvl
     * @return Folders
     */
    public function setLvl($lvl) {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getLvl() {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * @return Folders
     */
    public function setRgt($rgt) {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getRgt() {
        return $this->rgt;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * @return Folders
     */
    public function setLft($lft) {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer 
     */
    public function getLft() {
        return $this->lft;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Folders
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set foreignCode
     *
     * @param string $foreignCode
     * @return Folders
     */
    public function setForeignCode($foreignCode) {
        $this->foreignCode = $foreignCode;

        return $this;
    }

    /**
     * Get foreignCode
     *
     * @return string 
     */
    public function getForeignCode() {
        return $this->foreignCode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Folders
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Folders
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set visible
     *
     * @param string $visible
     * @return Folders
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string 
     */
    public function getVisible() {
        return $this->visible;
    }

    /**
     * Set allowordering
     *
     * @param string $allowordering
     * @return Folders
     */
    public function setAllowordering($allowordering) {
        $this->allowordering = $allowordering;

        return $this;
    }

    /**
     * Get allowordering
     *
     * @return string 
     */
    public function getAllowordering() {
        return $this->allowordering;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Folders
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set iconimagetype
     *
     * @param integer $iconimagetype
     * @return Folders
     */
    public function setIconimagetype($iconimagetype) {
        $this->iconimagetype = $iconimagetype;

        return $this;
    }

    /**
     * Get iconimagetype
     *
     * @return integer 
     */
    public function getIconimagetype() {
        return $this->iconimagetype;
    }

    /**
     * Set iconimagecolor
     *
     * @param integer $iconimagecolor
     * @return Folders
     */
    public function setIconimagecolor($iconimagecolor) {
        $this->iconimagecolor = $iconimagecolor;

        return $this;
    }

    /**
     * Get iconimagecolor
     *
     * @return integer 
     */
    public function getIconimagecolor() {
        return $this->iconimagecolor;
    }

    /**
     * Set customordering
     *
     * @param integer $customordering
     * @return Folders
     */
    public function setCustomordering($customordering) {
        $this->customordering = $customordering;

        return $this;
    }

    /**
     * Get customordering
     *
     * @return integer 
     */
    public function getCustomordering() {
        return $this->customordering;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Folders
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set menu
     *
     * @param \Sistema\IRMBundle\Entity\Menus $menu
     * @return Folders
     */
    public function setMenu(\Sistema\IRMBundle\Entity\Menus $menu = null) {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \Sistema\IRMBundle\Entity\Menus 
     */
    public function getMenu() {
        return $this->menu;
    }

    /**
     * Set image
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $image
     * @return Folders
     */
    public function setImage(\Sistema\IRMBundle\Entity\EntityImages $image = null) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Sistema\IRMBundle\Entity\EntityImages 
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Add products
     *
     * @param \Sistema\IRMBundle\Entity\Products $products
     * @return Folders
     */
    public function addProduct(\Sistema\IRMBundle\Entity\Products $products) {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Sistema\IRMBundle\Entity\Products $products
     */
    public function removeProduct(\Sistema\IRMBundle\Entity\Products $products) {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts() {
        return $this->products;
    }

    /**
     * Add children
     *
     * @param \Sistema\IRMBundle\Entity\Folders $children
     * @return Folders
     */
    public function addChild(\Sistema\IRMBundle\Entity\Folders $children) {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Sistema\IRMBundle\Entity\Folders $children
     */
    public function removeChild(\Sistema\IRMBundle\Entity\Folders $children) {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Sistema\IRMBundle\Entity\Folders $parent
     * @return Folders
     */
    public function setParent(\Sistema\IRMBundle\Entity\Folders $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Sistema\IRMBundle\Entity\Folders 
     */
    public function getParent() {
        return $this->parent;
    }

}