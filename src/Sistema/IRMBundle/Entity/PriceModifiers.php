<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * PriceModifiers
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\PriceModifiersRepository")
 * @Serializer\XmlRoot("priceModifiers")
 *
 * @Hateoas\Relation("self", href = "expr('/api/priceModifiers/' ~ object.getId())")
 */
class PriceModifiers {
    /** @Serializer\XmlAttribute */

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal")
     * @Assert\Type(type="decimal", message="The value {{ value }} is not a valid {{ type }}.")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Products", inversedBy="modifierPrices",cascade={"all"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * */
    private $product;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PriceModifiers
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PriceModifiers
     */
    public function setAmount($amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set product
     *
     * @param \Sistema\IRMBundle\Entity\Products $product
     * @return PriceModifiers
     */
    public function setProduct(\Sistema\IRMBundle\Entity\Products $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Sistema\IRMBundle\Entity\Products 
     */
    public function getProduct() {
        return $this->product;
    }

    public function getJson()
    {
        return $this->id . "^" . $this->name . "^" . $this->amount;
    }

}
