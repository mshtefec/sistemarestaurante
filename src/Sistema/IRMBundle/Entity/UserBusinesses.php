<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserBusinesses
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\UserBusinessesRepository")
 */
class UserBusinesses {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="userBusinesses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Businesses", inversedBy="userBusinesses")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     */
    private $business;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return User_businesses
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set business
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $business
     * @return User_businesses
     */
    public function setBusiness(\Sistema\IRMBundle\Entity\Businesses $business = null) {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Sistema\IRMBundle\Entity\Businesses 
     */
    public function getBusiness() {
        return $this->business;
    }

    /**
     * Set role
     *
     * @param \Sistema\UserBundle\Entity\Role $role
     * @return User_businesses
     */
    public function setRole(\Sistema\UserBundle\Entity\Role $role = null) {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Sistema\UserBundle\Entity\Role 
     */
    public function getRole() {
        return $this->role;
    }

}
