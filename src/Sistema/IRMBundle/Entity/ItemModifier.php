<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * ItemModifier
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\ItemModifierRepository")
 * @Serializer\XmlRoot("itemmodifier")
 *
 * @Hateoas\Relation("self", href = "expr('/api/itemmodifiers/' ~ object.getId())")
 */
class ItemModifier {
    /** @Serializer\XmlAttribute */

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal")
     * @Assert\Type(type="decimal", message="The value {{ value }} is not a valid {{ type }}.")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="quality", type="decimal")
     * @Assert\Type(type="decimal", message="The value {{ value }} is not a valid {{ type }}.")
     */
    private $quality;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\OrderItems", inversedBy="itemModifiers" ,cascade={"all"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * */
    private $orderItem;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ItemModifier
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return ItemModifier
     */
    public function setAmount($amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set quality
     *
     * @param string $quality
     * @return ItemModifier
     */
    public function setQuality($quality) {
        $this->quality = $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return string 
     */
    public function getQuality() {
        return $this->quality;
    }

    /**
     * Set orderItem
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItem
     * @return ItemModifier
     */
    public function setOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItem = null) {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get orderItem
     *
     * @return \Sistema\IRMBundle\Entity\OrderItems 
     */
    public function getOrderItem() {
        return $this->orderItem;
    }

}
