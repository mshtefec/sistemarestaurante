<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\OrdersRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Orders extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="deviceUUID", type="string", length=50, nullable=true)
     */
    private $deviceuuid;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=200, nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="deliveryDate", type="string", length=200, nullable=true)
     */
    private $deliverydate;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="taxName", type="string", length=15, nullable=true)
     */
    private $taxname;

    /**
     * @var float
     *
     * @ORM\Column(name="taxValue", type="float", precision=10, scale=0, nullable=true)
     */
    private $taxvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="guestsQuantity", type="integer", nullable=true)
     */
    private $guestsquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\Businesses
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Businesses")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     */
    private $business;

    /**
     * @var \Sistema\IRMBundle\Entity\Tables
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Tables")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="table_id", referencedColumnName="id")
     * })
     */
    private $table;

    /**
     * @var string
     *
     * @ORM\Column(name="user_address", type="string", length=200, nullable=true)
     */
    private $userAddress;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\OrderItems", mappedBy="order" ,cascade={"all"})
     * */
    private $orderItems;

    /**
     * Constructor
     */
    public function __construct() {
        $this->orderItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Orders
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set deviceuuid
     *
     * @param string $deviceuuid
     * @return Orders
     */
    public function setDeviceuuid($deviceuuid) {
        $this->deviceuuid = $deviceuuid;

        return $this;
    }

    /**
     * Get deviceuuid
     *
     * @return string 
     */
    public function getDeviceuuid() {
        return $this->deviceuuid;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Orders
     */
    public function setNote($note) {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote() {
        return $this->note;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Orders
     */
    public function setAmount($amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Set taxname
     *
     * @param string $taxname
     * @return Orders
     */
    public function setTaxname($taxname) {
        $this->taxname = $taxname;

        return $this;
    }

    /**
     * Get taxname
     *
     * @return string 
     */
    public function getTaxname() {
        return $this->taxname;
    }

    /**
     * Set taxvalue
     *
     * @param float $taxvalue
     * @return Orders
     */
    public function setTaxvalue($taxvalue) {
        $this->taxvalue = $taxvalue;

        return $this;
    }

    /**
     * Get taxvalue
     *
     * @return float 
     */
    public function getTaxvalue() {
        return $this->taxvalue;
    }

    /**
     * Set guestsquantity
     *
     * @param integer $guestsquantity
     * @return Orders
     */
    public function setGuestsquantity($guestsquantity) {
        $this->guestsquantity = $guestsquantity;

        return $this;
    }

    /**
     * Get guestsquantity
     *
     * @return integer 
     */
    public function getGuestsquantity() {
        return $this->guestsquantity;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Orders
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Orders
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Orders
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set business
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $business
     * @return Orders
     */
    public function setBusiness(\Sistema\IRMBundle\Entity\Businesses $business = null) {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Sistema\IRMBundle\Entity\Businesses 
     */
    public function getBusiness() {
        return $this->business;
    }

    /**
     * Set table
     *
     * @param \Sistema\IRMBundle\Entity\Tables $table
     * @return Orders
     */
    public function setTable(\Sistema\IRMBundle\Entity\Tables $table = null) {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return \Sistema\IRMBundle\Entity\Tables 
     */
    public function getTable() {
        return $this->table;
    }

    /**
     * Set userAddress
     *
     * @param string $userAddress
     * @return Orders
     */
    public function setUseraddress($userAddress) {
        $this->userAddress = $userAddress;

        return $this;
    }

    /**
     * Get userAddress
     *
     * @return string 
     */
    public function getUseraddress() {
        return $this->userAddress;
    }

    /**
     * Add orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     * @return Orders
     */
    public function addOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $orderItems->setOrder($this);
        $this->orderItems[] = $orderItems;

        return $this;
    }

    /**
     * Remove orderItems
     *
     * @param \Sistema\IRMBundle\Entity\OrderItems $orderItems
     */
    public function removeOrderItem(\Sistema\IRMBundle\Entity\OrderItems $orderItems) {
        $this->orderItems->removeElement($orderItems);
    }

    /**
     * Get orderItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderItems() {
        return $this->orderItems;
    }


    /**
     * Set deliverydate
     *
     * @param string $deliverydate
     * @return Orders
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return string 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }
}
