<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * OrderItems
 *
 * @ORM\Table(name="order_items")
 * @ORM\Entity
 */
class OrderItems extends MWSgedmo{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="selectedPriceName", type="string", length=25, nullable=true)
     */
    private $selectedpricename;

    /**
     * @var float
     *
     * @ORM\Column(name="selectedPriceAmount", type="float", precision=10, scale=0, nullable=true)
     */
    private $selectedpriceamount;

    /**
     * @var float
     *
     * @ORM\Column(name="customizedPriceAmount", type="float", precision=10, scale=0, nullable=true)
     */
    private $customizedpriceamount;

    /**
     * @var string
     *
     * @ORM\Column(name="modifierArraySerialized", type="json_array", nullable=true)
     */
    private $modifierarrayserialized;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", precision=10, scale=0, nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=40, nullable=true)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=70, nullable=true)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\Businesses
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Businesses", inversedBy="orderItems")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     */
    private $business;

    /**
     * @var \Sistema\IRMBundle\Entity\Products
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Products", inversedBy="orderItems",cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Sistema\IRMBundle\Entity\Orders
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Orders", inversedBy="orderItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\ItemModifier", mappedBy="orderItem" ,cascade={"all"})
     * */
    private $itemModifiers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itemModifiers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return OrderItems
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set selectedpricename
     *
     * @param string $selectedpricename
     * @return OrderItems
     */
    public function setSelectedpricename($selectedpricename)
    {
        $this->selectedpricename = $selectedpricename;

        return $this;
    }

    /**
     * Get selectedpricename
     *
     * @return string 
     */
    public function getSelectedpricename()
    {
        return $this->selectedpricename;
    }

    /**
     * Set selectedpriceamount
     *
     * @param float $selectedpriceamount
     * @return OrderItems
     */
    public function setSelectedpriceamount($selectedpriceamount)
    {
        $this->selectedpriceamount = $selectedpriceamount;

        return $this;
    }

    /**
     * Get selectedpriceamount
     *
     * @return float 
     */
    public function getSelectedpriceamount()
    {
        return $this->selectedpriceamount;
    }

    /**
     * Set customizedpriceamount
     *
     * @param float $customizedpriceamount
     * @return OrderItems
     */
    public function setCustomizedpriceamount($customizedpriceamount)
    {
        $this->customizedpriceamount = $customizedpriceamount;

        return $this;
    }

    /**
     * Get customizedpriceamount
     *
     * @return float 
     */
    public function getCustomizedpriceamount()
    {
        return $this->customizedpriceamount;
    }

    /**
     * Set modifierarrayserialized
     *
     * @param array $modifierarrayserialized
     * @return OrderItems
     */
    public function setModifierarrayserialized($modifierarrayserialized)
    {
        $this->modifierarrayserialized = $modifierarrayserialized;

        return $this;
    }

    /**
     * Get modifierarrayserialized
     *
     * @return array 
     */
    public function getModifierarrayserialized()
    {
        return $this->modifierarrayserialized;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return OrderItems
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return OrderItems
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return OrderItems
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OrderItems
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return OrderItems
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return OrderItems
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set business
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $business
     * @return OrderItems
     */
    public function setBusiness(\Sistema\IRMBundle\Entity\Businesses $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Sistema\IRMBundle\Entity\Businesses 
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * Set product
     *
     * @param \Sistema\IRMBundle\Entity\Products $product
     * @return OrderItems
     */
    public function setProduct(\Sistema\IRMBundle\Entity\Products $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Sistema\IRMBundle\Entity\Products 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @param \Sistema\IRMBundle\Entity\Orders $order
     * @return OrderItems
     */
    public function setOrder(\Sistema\IRMBundle\Entity\Orders $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Sistema\IRMBundle\Entity\Orders 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Add itemModifiers
     *
     * @param \Sistema\IRMBundle\Entity\ItemModifier $itemModifiers
     * @return OrderItems
     */
    public function addItemModifier(\Sistema\IRMBundle\Entity\ItemModifier $itemModifiers)
    {
        $itemModifiers->setOrderItem($this);
        $this->itemModifiers[] = $itemModifiers;

        return $this;
    }

    /**
     * Remove itemModifiers
     *
     * @param \Sistema\IRMBundle\Entity\ItemModifier $itemModifiers
     */
    public function removeItemModifier(\Sistema\IRMBundle\Entity\ItemModifier $itemModifiers)
    {
        $this->itemModifiers->removeElement($itemModifiers);
    }

    /**
     * Get itemModifiers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItemModifiers()
    {
        return $this->itemModifiers;
    }
}
