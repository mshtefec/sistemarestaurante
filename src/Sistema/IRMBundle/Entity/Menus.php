<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\IRMBundle\Filter\menuInterface;

/**
 * Menus
 *
 * @ORM\Table(name="menus")
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\MenusRepository")
 */
class Menus extends MWSgedmo implements menuInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="syncProcessTimeStamp", type="bigint", nullable=false)
     */
    private $syncprocesstimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="menus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Businesses", mappedBy="menu")
     * */
    private $businesses;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Folders", mappedBy="menu",cascade={"all"})
     * */
    private $folders;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Products", mappedBy="menu")
     * */
    private $products;

    public function __toString() {
        return $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->businesses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->folders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->syncprocesstimestamp = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Menus
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Menus
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Menus
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set syncprocesstimestamp
     *
     * @param integer $syncprocesstimestamp
     * @return Menus
     */
    public function setSyncprocesstimestamp($syncprocesstimestamp) {
        $this->syncprocesstimestamp = $syncprocesstimestamp;

        return $this;
    }

    /**
     * Get syncprocesstimestamp
     *
     * @return integer 
     */
    public function getSyncprocesstimestamp() {
        return $this->syncprocesstimestamp;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Menus
     */
    public function setIsdeleted($isdeleted) {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted() {
        return $this->isdeleted;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Menus
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add businesses
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $businesses
     * @return Menus
     */
    public function addBusiness(\Sistema\IRMBundle\Entity\Businesses $businesses) {
        $businesses->setMenu($this);
        $this->businesses[] = $businesses;

        return $this;
    }

    /**
     * Remove businesses
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $businesses
     */
    public function removeBusiness(\Sistema\IRMBundle\Entity\Businesses $businesses) {
        $this->businesses->removeElement($businesses);
    }

    /**
     * Get businesses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBusinesses() {
        return $this->businesses;
    }

    /**
     * Add folders
     *
     * @param \Sistema\IRMBundle\Entity\Folders $folders
     * @return Menus
     */
    public function addFolder(\Sistema\IRMBundle\Entity\Folders $folders) {
        $folders->setMenu($this);
        $this->folders[] = $folders;

        return $this;
    }

    /**
     * Remove folders
     *
     * @param \Sistema\IRMBundle\Entity\Folders $folders
     */
    public function removeFolder(\Sistema\IRMBundle\Entity\Folders $folders) {
        $this->folders->removeElement($folders);
    }

    /**
     * Get folders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFolders() {
        return $this->folders;
    }

    /**
     * Add products
     *
     * @param \Sistema\IRMBundle\Entity\Products $products
     * @return Menus
     */
    public function addProduct(\Sistema\IRMBundle\Entity\Products $products) {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Sistema\IRMBundle\Entity\Products $products
     */
    public function removeProduct(\Sistema\IRMBundle\Entity\Products $products) {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts() {
        return $this->products;
    }

}
