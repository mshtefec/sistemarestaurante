<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\IRMBundle\Filter\negocioInterface;

/**
 * Tables
 *
 * @ORM\Table(name="tables")
 * @ORM\Entity
 */
class Tables extends MWSgedmo implements negocioInterface{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=true)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="isDeleted", type="integer", nullable=false)
     */
    private $isdeleted;

    /**
     * @var \Sistema\IRMBundle\Entity\EntityImages
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\EntityImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var \Sistema\IRMBundle\Entity\Businesses
     *
     * @ORM\ManyToOne(targetEntity="Sistema\IRMBundle\Entity\Businesses", inversedBy="tables")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     */
    private $business;

  
    public function __toString() {
        return $this->getName();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Tables
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tables
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Tables
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Tables
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Tables
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isdeleted
     *
     * @param integer $isdeleted
     * @return Tables
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return integer 
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set image
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $image
     * @return Tables
     */
    public function setImage(\Sistema\IRMBundle\Entity\EntityImages $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Sistema\IRMBundle\Entity\EntityImages 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set business
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $business
     * @return Tables
     */
    public function setBusiness(\Sistema\IRMBundle\Entity\Businesses $business = null)
    {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Sistema\IRMBundle\Entity\Businesses 
     */
    public function getBusiness()
    {
        return $this->business;
    }
}
