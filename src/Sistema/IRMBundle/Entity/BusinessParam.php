<?php

namespace Sistema\IRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BusinessParam
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\IRMBundle\Entity\BusinessParamRepository")
 */
class BusinessParam {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="currencySimbol", type="string", length=255, nullable=true)
     */
    private $currencySimbol;

    /**
     * @var boolean
     *
     * @ORM\Column(name="applyTax", type="boolean", nullable=true)
     * 
     */
    private $applyTax;

    /**
     * @var string
     *
     * @ORM\Column(name="taxArraySerialized", type="string", length=255, nullable=true)
     */
    private $taxArraySerialized;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showTax", type="boolean", nullable=true)
     */
    private $showTax;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Tax", mappedBy="businessParam" , cascade={"all"})
     */
    private $taxs;

    /**
     * @var string
     *
     * @ORM\Column(name="catalogNameToShow", type="string", length=255, nullable=true)
     */
    private $catalogNameToShow;

    /**
     * @var string
     *
     * @ORM\Column(name="colorTheme", type="string", length=255, nullable=true)
     */
    private $colorTheme;

    /**
     * @var integer
     *
     * @ORM\Column(name="catalogLayout", type="integer", nullable=true)
     */
    private $catalogLayout;

    /**
     * @var string
     *
     * @ORM\Column(name="ShowSplashBusinessDescription", type="boolean", nullable=true)
     */
    private $showSplashBusinessDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showCategoryIcon", type="boolean", nullable=true)
     */
    private $showCategoryIcon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showSplahBusinessIcon", type="boolean", nullable=true)
     */
    private $showSplahBusinessIcon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showCategoryDescription", type="boolean", nullable=true)
     */
    private $showCategoryDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showSubCategoryIcon", type="boolean", nullable=true)
     */
    private $showSubCategoryIcon;

    /**
     * @var string
     *
     * @ORM\Column(name="showSubCategoryDescription", type="string", length=255, nullable=true)
     */
    private $showSubCategoryDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordeningOfCategories", type="integer", nullable=true)
     */
    private $ordeningOfCategories;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordeningOfSubCategories", type="integer", nullable=true)
     */
    private $ordeningOfSubCategories;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderingOfItems", type="integer", nullable=true)
     */
    private $orderingOfItems;

    /**
     * @var integer
     *
     * @ORM\Column(name="typeViewOfItemsOfStandardView", type="integer", nullable=true)
     */
    private $typeViewOfItemsOfStandardView;

    /**
     * @var integer
     *
     * @ORM\Column(name="rowsOfItemsPerCategory", type="integer", nullable=true)
     */
    private $rowsOfItemsPerCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="decimalSeparator", type="string", length=255, nullable=true)
     */
    private $decimalSeparator;

    /**
     * @var integer
     *
     * @ORM\Column(name="decimalDigits", type="integer", nullable=true)
     */
    private $decimalDigits;

    /**
     * @var string
     *
     * @ORM\Column(name="thousandsSeparator", type="string", length=255, nullable=true)
     */
    private $thousandsSeparator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showPricesPrivateProfile", type="boolean", nullable=true)
     */
    private $showPricesPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showPricePublicProfile", type="boolean", nullable=true)
     */
    private $showPricePublicProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="workflowForTakingOrdersPublicProfile", type="boolean", nullable=true)
     */
    private $workflowForTakingOrdersPublicProfile;

    /**
     * @var integer
     *
     * @ORM\Column(name="workflowForTakingOrdersPrivateProfile", type="integer", nullable=true)
     */
    private $workflowForTakingOrdersPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowTakingOrdersByGuestsPrivateProfile", type="boolean", nullable=true)
     */
    private $allowTakingOrdersByGuestsPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowTakingOrdersByGuestPublicProfile", type="boolean", nullable=true)
     */
    private $allowTakingOrdersByGuestPublicProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowEnteringGuestIdentifierPrivateProfile", type="boolean", nullable=true)
     */
    private $allowEnteringGuestIdentifierPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowEnteringGuestIdentifierPublicProfile", type="boolean", nullable=true)
     */
    private $allowEnteringGuestIdentifierPublicProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowEnteringDeliveryDatePrivateProfile", type="boolean", nullable=true)
     */
    private $allowEnteringDeliveryDatePrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowEnteringDeliveryDatePublicProfile", type="boolean", nullable=true)
     */
    private $allowEnteringDeliveryDatePublicProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showWhereAreWeShortcutPrivateProfile", type="boolean", nullable=true)
     */
    private $showWhereAreWeShortcutPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showInformationShortcutPrivateProfile", type="boolean", nullable=true)
     */
    private $showInformationShortcutPrivateProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showWhereAreWeShorcutPublicProfile", type="boolean", nullable=true)
     */
    private $showWhereAreWeShorcutPublicProfile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="showInformationShorcutPublicProfile", type="boolean", nullable=true)
     */
    private $showInformationShorcutPublicProfile;

    /**
     * @var string
     *
     * @ORM\Column(name="dateFormat", type="string", length=255, nullable=true)
     */
    private $dateFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="timeZoneId", type="string", length=255, nullable=true)
     */
    private $timeZoneId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticketShowBusinessIcon", type="boolean", nullable=true)
     */
    private $ticketShowBusinessIcon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticketShowBusinessAddress", type="boolean", nullable=true)
     */
    private $ticketShowBusinessAddress;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticketShowBusinessMainPhone", type="boolean", nullable=true)
     */
    private $ticketShowBusinessMainPhone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticketShowBusinessContactEmail", type="boolean", nullable=true)
     */
    private $ticketShowBusinessContactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketShowFreeTextHeader", type="boolean", nullable=true)
     */
    private $ticketShowFreeTextHeader;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketFreeTextHeader", type="string", length=255, nullable=true)
     */
    private $ticketFreeTextHeader;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticcketShowFreeTextFooter", type="boolean", nullable=true)
     */
    private $ticcketShowFreeTextFooter;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketFreeTextFooter", type="string", length=255, nullable=true)
     */
    private $ticketFreeTextFooter;

    /**
     * @ORM\OneToOne(targetEntity="Businesses", inversedBy="businessParam")
     * @ORM\JoinColumn(name="business_id", referencedColumnName="id")
     * */
    private $business;

    /**
     * Constructor
     */
    public function __construct() {
        $this->taxs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set currencySimbol
     *
     * @param string $currencySimbol
     * @return BusinessParam
     */
    public function setCurrencySimbol($currencySimbol) {
        $this->currencySimbol = $currencySimbol;

        return $this;
    }

    /**
     * Get currencySimbol
     *
     * @return string 
     */
    public function getCurrencySimbol() {
        return $this->currencySimbol;
    }

    /**
     * Set applyTax
     *
     * @param boolean $applyTax
     * @return BusinessParam
     */
    public function setApplyTax($applyTax) {
        $this->applyTax = $applyTax;

        return $this;
    }

    /**
     * Get applyTax
     *
     * @return boolean 
     */
    public function getApplyTax() {
        return $this->applyTax;
    }

    /**
     * Set taxArraySerialized
     *
     * @param string $taxArraySerialized
     * @return BusinessParam
     */
    public function setTaxArraySerialized($taxArraySerialized) {
        $this->taxArraySerialized = $taxArraySerialized;

        return $this;
    }

    /**
     * Get taxArraySerialized
     *
     * @return string 
     */
    public function getTaxArraySerialized() {
        return $this->taxArraySerialized;
    }

    /**
     * Set showTax
     *
     * @param boolean $showTax
     * @return BusinessParam
     */
    public function setShowTax($showTax) {
        $this->showTax = $showTax;

        return $this;
    }

    /**
     * Get showTax
     *
     * @return boolean 
     */
    public function getShowTax() {
        return $this->showTax;
    }

    /**
     * Set catalogNameToShow
     *
     * @param string $catalogNameToShow
     * @return BusinessParam
     */
    public function setCatalogNameToShow($catalogNameToShow) {
        $this->catalogNameToShow = $catalogNameToShow;

        return $this;
    }

    /**
     * Get catalogNameToShow
     *
     * @return string 
     */
    public function getCatalogNameToShow() {
        return $this->catalogNameToShow;
    }

    /**
     * Set colorTheme
     *
     * @param string $colorTheme
     * @return BusinessParam
     */
    public function setColorTheme($colorTheme) {
        $this->colorTheme = $colorTheme;

        return $this;
    }

    /**
     * Get colorTheme
     *
     * @return string 
     */
    public function getColorTheme() {
        return $this->colorTheme;
    }

    /**
     * Set catalogLayout
     *
     * @param integer $catalogLayout
     * @return BusinessParam
     */
    public function setCatalogLayout($catalogLayout) {
        $this->catalogLayout = $catalogLayout;

        return $this;
    }

    /**
     * Get catalogLayout
     *
     * @return integer 
     */
    public function getCatalogLayout() {
        return $this->catalogLayout;
    }

    /**
     * Set showSplashBusinessDescription
     *
     * @param boolean $showSplashBusinessDescription
     * @return BusinessParam
     */
    public function setShowSplashBusinessDescription($showSplashBusinessDescription) {
        $this->showSplashBusinessDescription = $showSplashBusinessDescription;

        return $this;
    }

    /**
     * Get showSplashBusinessDescription
     *
     * @return boolean 
     */
    public function getShowSplashBusinessDescription() {
        return $this->showSplashBusinessDescription;
    }

    /**
     * Set showCategoryIcon
     *
     * @param boolean $showCategoryIcon
     * @return BusinessParam
     */
    public function setShowCategoryIcon($showCategoryIcon) {
        $this->showCategoryIcon = $showCategoryIcon;

        return $this;
    }

    /**
     * Get showCategoryIcon
     *
     * @return boolean 
     */
    public function getShowCategoryIcon() {
        return $this->showCategoryIcon;
    }

    /**
     * Set showSplahBusinessIcon
     *
     * @param boolean $showSplahBusinessIcon
     * @return BusinessParam
     */
    public function setShowSplahBusinessIcon($showSplahBusinessIcon) {
        $this->showSplahBusinessIcon = $showSplahBusinessIcon;

        return $this;
    }

    /**
     * Get showSplahBusinessIcon
     *
     * @return boolean 
     */
    public function getShowSplahBusinessIcon() {
        return $this->showSplahBusinessIcon;
    }

    /**
     * Set showCategoryDescription
     *
     * @param boolean $showCategoryDescription
     * @return BusinessParam
     */
    public function setShowCategoryDescription($showCategoryDescription) {
        $this->showCategoryDescription = $showCategoryDescription;

        return $this;
    }

    /**
     * Get showCategoryDescription
     *
     * @return boolean 
     */
    public function getShowCategoryDescription() {
        return $this->showCategoryDescription;
    }

    /**
     * Set showSubCategoryIcon
     *
     * @param boolean $showSubCategoryIcon
     * @return BusinessParam
     */
    public function setShowSubCategoryIcon($showSubCategoryIcon) {
        $this->showSubCategoryIcon = $showSubCategoryIcon;

        return $this;
    }

    /**
     * Get showSubCategoryIcon
     *
     * @return boolean 
     */
    public function getShowSubCategoryIcon() {
        return $this->showSubCategoryIcon;
    }

    /**
     * Set showSubCategoryDescription
     *
     * @param string $showSubCategoryDescription
     * @return BusinessParam
     */
    public function setShowSubCategoryDescription($showSubCategoryDescription) {
        $this->showSubCategoryDescription = $showSubCategoryDescription;

        return $this;
    }

    /**
     * Get showSubCategoryDescription
     *
     * @return string 
     */
    public function getShowSubCategoryDescription() {
        return $this->showSubCategoryDescription;
    }

    /**
     * Set ordeningOfCategories
     *
     * @param integer $ordeningOfCategories
     * @return BusinessParam
     */
    public function setOrdeningOfCategories($ordeningOfCategories) {
        $this->ordeningOfCategories = $ordeningOfCategories;

        return $this;
    }

    /**
     * Get ordeningOfCategories
     *
     * @return integer 
     */
    public function getOrdeningOfCategories() {
        return $this->ordeningOfCategories;
    }

    /**
     * Set ordeningOfSubCategories
     *
     * @param integer $ordeningOfSubCategories
     * @return BusinessParam
     */
    public function setOrdeningOfSubCategories($ordeningOfSubCategories) {
        $this->ordeningOfSubCategories = $ordeningOfSubCategories;

        return $this;
    }

    /**
     * Get ordeningOfSubCategories
     *
     * @return integer 
     */
    public function getOrdeningOfSubCategories() {
        return $this->ordeningOfSubCategories;
    }

    /**
     * Set orderingOfItems
     *
     * @param integer $orderingOfItems
     * @return BusinessParam
     */
    public function setOrderingOfItems($orderingOfItems) {
        $this->orderingOfItems = $orderingOfItems;

        return $this;
    }

    /**
     * Get orderingOfItems
     *
     * @return integer 
     */
    public function getOrderingOfItems() {
        return $this->orderingOfItems;
    }

    /**
     * Set typeViewOfItemsOfStandardView
     *
     * @param integer $typeViewOfItemsOfStandardView
     * @return BusinessParam
     */
    public function setTypeViewOfItemsOfStandardView($typeViewOfItemsOfStandardView) {
        $this->typeViewOfItemsOfStandardView = $typeViewOfItemsOfStandardView;

        return $this;
    }

    /**
     * Get typeViewOfItemsOfStandardView
     *
     * @return integer 
     */
    public function getTypeViewOfItemsOfStandardView() {
        return $this->typeViewOfItemsOfStandardView;
    }

    /**
     * Set rowsOfItemsPerCategory
     *
     * @param integer $rowsOfItemsPerCategory
     * @return BusinessParam
     */
    public function setRowsOfItemsPerCategory($rowsOfItemsPerCategory) {
        $this->rowsOfItemsPerCategory = $rowsOfItemsPerCategory;

        return $this;
    }

    /**
     * Get rowsOfItemsPerCategory
     *
     * @return integer 
     */
    public function getRowsOfItemsPerCategory() {
        return $this->rowsOfItemsPerCategory;
    }

    /**
     * Set decimalSeparator
     *
     * @param string $decimalSeparator
     * @return BusinessParam
     */
    public function setDecimalSeparator($decimalSeparator) {
        $this->decimalSeparator = $decimalSeparator;

        return $this;
    }

    /**
     * Get decimalSeparator
     *
     * @return string 
     */
    public function getDecimalSeparator() {
        return $this->decimalSeparator;
    }

    /**
     * Set decimalDigits
     *
     * @param integer $decimalDigits
     * @return BusinessParam
     */
    public function setDecimalDigits($decimalDigits) {
        $this->decimalDigits = $decimalDigits;

        return $this;
    }

    /**
     * Get decimalDigits
     *
     * @return integer 
     */
    public function getDecimalDigits() {
        return $this->decimalDigits;
    }

    /**
     * Set thousandsSeparator
     *
     * @param string $thousandsSeparator
     * @return BusinessParam
     */
    public function setThousandsSeparator($thousandsSeparator) {
        $this->thousandsSeparator = $thousandsSeparator;

        return $this;
    }

    /**
     * Get thousandsSeparator
     *
     * @return string 
     */
    public function getThousandsSeparator() {
        return $this->thousandsSeparator;
    }

    /**
     * Set showPricesPrivateProfile
     *
     * @param boolean $showPricesPrivateProfile
     * @return BusinessParam
     */
    public function setShowPricesPrivateProfile($showPricesPrivateProfile) {
        $this->showPricesPrivateProfile = $showPricesPrivateProfile;

        return $this;
    }

    /**
     * Get showPricesPrivateProfile
     *
     * @return boolean 
     */
    public function getShowPricesPrivateProfile() {
        return $this->showPricesPrivateProfile;
    }

    /**
     * Set showPricePublicProfile
     *
     * @param boolean $showPricePublicProfile
     * @return BusinessParam
     */
    public function setShowPricePublicProfile($showPricePublicProfile) {
        $this->showPricePublicProfile = $showPricePublicProfile;

        return $this;
    }

    /**
     * Get showPricePublicProfile
     *
     * @return boolean 
     */
    public function getShowPricePublicProfile() {
        return $this->showPricePublicProfile;
    }

    /**
     * Set workflowForTakingOrdersPublicProfile
     *
     * @param boolean $workflowForTakingOrdersPublicProfile
     * @return BusinessParam
     */
    public function setWorkflowForTakingOrdersPublicProfile($workflowForTakingOrdersPublicProfile) {
        $this->workflowForTakingOrdersPublicProfile = $workflowForTakingOrdersPublicProfile;

        return $this;
    }

    /**
     * Get workflowForTakingOrdersPublicProfile
     *
     * @return boolean 
     */
    public function getWorkflowForTakingOrdersPublicProfile() {
        return $this->workflowForTakingOrdersPublicProfile;
    }

    /**
     * Set workflowForTakingOrdersPrivateProfile
     *
     * @param integer $workflowForTakingOrdersPrivateProfile
     * @return BusinessParam
     */
    public function setWorkflowForTakingOrdersPrivateProfile($workflowForTakingOrdersPrivateProfile) {
        $this->workflowForTakingOrdersPrivateProfile = $workflowForTakingOrdersPrivateProfile;

        return $this;
    }

    /**
     * Get workflowForTakingOrdersPrivateProfile
     *
     * @return integer 
     */
    public function getWorkflowForTakingOrdersPrivateProfile() {
        return $this->workflowForTakingOrdersPrivateProfile;
    }

    /**
     * Set allowTakingOrdersByGuestsPrivateProfile
     *
     * @param boolean $allowTakingOrdersByGuestsPrivateProfile
     * @return BusinessParam
     */
    public function setAllowTakingOrdersByGuestsPrivateProfile($allowTakingOrdersByGuestsPrivateProfile) {
        $this->allowTakingOrdersByGuestsPrivateProfile = $allowTakingOrdersByGuestsPrivateProfile;

        return $this;
    }

    /**
     * Get allowTakingOrdersByGuestsPrivateProfile
     *
     * @return boolean 
     */
    public function getAllowTakingOrdersByGuestsPrivateProfile() {
        return $this->allowTakingOrdersByGuestsPrivateProfile;
    }

    /**
     * Set allowTakingOrdersByGuestPublicProfile
     *
     * @param boolean $allowTakingOrdersByGuestPublicProfile
     * @return BusinessParam
     */
    public function setAllowTakingOrdersByGuestPublicProfile($allowTakingOrdersByGuestPublicProfile) {
        $this->allowTakingOrdersByGuestPublicProfile = $allowTakingOrdersByGuestPublicProfile;

        return $this;
    }

    /**
     * Get allowTakingOrdersByGuestPublicProfile
     *
     * @return boolean 
     */
    public function getAllowTakingOrdersByGuestPublicProfile() {
        return $this->allowTakingOrdersByGuestPublicProfile;
    }

    /**
     * Set allowEnteringGuestIdentifierPrivateProfile
     *
     * @param boolean $allowEnteringGuestIdentifierPrivateProfile
     * @return BusinessParam
     */
    public function setAllowEnteringGuestIdentifierPrivateProfile($allowEnteringGuestIdentifierPrivateProfile) {
        $this->allowEnteringGuestIdentifierPrivateProfile = $allowEnteringGuestIdentifierPrivateProfile;

        return $this;
    }

    /**
     * Get allowEnteringGuestIdentifierPrivateProfile
     *
     * @return boolean 
     */
    public function getAllowEnteringGuestIdentifierPrivateProfile() {
        return $this->allowEnteringGuestIdentifierPrivateProfile;
    }

    /**
     * Set allowEnteringGuestIdentifierPublicProfile
     *
     * @param boolean $allowEnteringGuestIdentifierPublicProfile
     * @return BusinessParam
     */
    public function setAllowEnteringGuestIdentifierPublicProfile($allowEnteringGuestIdentifierPublicProfile) {
        $this->allowEnteringGuestIdentifierPublicProfile = $allowEnteringGuestIdentifierPublicProfile;

        return $this;
    }

    /**
     * Get allowEnteringGuestIdentifierPublicProfile
     *
     * @return boolean 
     */
    public function getAllowEnteringGuestIdentifierPublicProfile() {
        return $this->allowEnteringGuestIdentifierPublicProfile;
    }

    /**
     * Set allowEnteringDeliveryDatePrivateProfile
     *
     * @param boolean $allowEnteringDeliveryDatePrivateProfile
     * @return BusinessParam
     */
    public function setAllowEnteringDeliveryDatePrivateProfile($allowEnteringDeliveryDatePrivateProfile) {
        $this->allowEnteringDeliveryDatePrivateProfile = $allowEnteringDeliveryDatePrivateProfile;

        return $this;
    }

    /**
     * Get allowEnteringDeliveryDatePrivateProfile
     *
     * @return boolean 
     */
    public function getAllowEnteringDeliveryDatePrivateProfile() {
        return $this->allowEnteringDeliveryDatePrivateProfile;
    }

    /**
     * Set allowEnteringDeliveryDatePublicProfile
     *
     * @param boolean $allowEnteringDeliveryDatePublicProfile
     * @return BusinessParam
     */
    public function setAllowEnteringDeliveryDatePublicProfile($allowEnteringDeliveryDatePublicProfile) {
        $this->allowEnteringDeliveryDatePublicProfile = $allowEnteringDeliveryDatePublicProfile;

        return $this;
    }

    /**
     * Get allowEnteringDeliveryDatePublicProfile
     *
     * @return boolean 
     */
    public function getAllowEnteringDeliveryDatePublicProfile() {
        return $this->allowEnteringDeliveryDatePublicProfile;
    }

    /**
     * Set showWhereAreWeShortcutPrivateProfile
     *
     * @param boolean $showWhereAreWeShortcutPrivateProfile
     * @return BusinessParam
     */
    public function setShowWhereAreWeShortcutPrivateProfile($showWhereAreWeShortcutPrivateProfile) {
        $this->showWhereAreWeShortcutPrivateProfile = $showWhereAreWeShortcutPrivateProfile;

        return $this;
    }

    /**
     * Get showWhereAreWeShortcutPrivateProfile
     *
     * @return boolean 
     */
    public function getShowWhereAreWeShortcutPrivateProfile() {
        return $this->showWhereAreWeShortcutPrivateProfile;
    }

    /**
     * Set showInformationShortcutPrivateProfile
     *
     * @param boolean $showInformationShortcutPrivateProfile
     * @return BusinessParam
     */
    public function setShowInformationShortcutPrivateProfile($showInformationShortcutPrivateProfile) {
        $this->showInformationShortcutPrivateProfile = $showInformationShortcutPrivateProfile;

        return $this;
    }

    /**
     * Get showInformationShortcutPrivateProfile
     *
     * @return boolean 
     */
    public function getShowInformationShortcutPrivateProfile() {
        return $this->showInformationShortcutPrivateProfile;
    }

    /**
     * Set showWhereAreWeShorcutPublicProfile
     *
     * @param boolean $showWhereAreWeShorcutPublicProfile
     * @return BusinessParam
     */
    public function setShowWhereAreWeShorcutPublicProfile($showWhereAreWeShorcutPublicProfile) {
        $this->showWhereAreWeShorcutPublicProfile = $showWhereAreWeShorcutPublicProfile;

        return $this;
    }

    /**
     * Get showWhereAreWeShorcutPublicProfile
     *
     * @return boolean 
     */
    public function getShowWhereAreWeShorcutPublicProfile() {
        return $this->showWhereAreWeShorcutPublicProfile;
    }

    /**
     * Set showInformationShorcutPublicProfile
     *
     * @param boolean $showInformationShorcutPublicProfile
     * @return BusinessParam
     */
    public function setShowInformationShorcutPublicProfile($showInformationShorcutPublicProfile) {
        $this->showInformationShorcutPublicProfile = $showInformationShorcutPublicProfile;

        return $this;
    }

    /**
     * Get showInformationShorcutPublicProfile
     *
     * @return boolean 
     */
    public function getShowInformationShorcutPublicProfile() {
        return $this->showInformationShorcutPublicProfile;
    }

    /**
     * Set dateFormat
     *
     * @param string $dateFormat
     * @return BusinessParam
     */
    public function setDateFormat($dateFormat) {
        $this->dateFormat = $dateFormat;

        return $this;
    }

    /**
     * Get dateFormat
     *
     * @return string 
     */
    public function getDateFormat() {
        return $this->dateFormat;
    }

    /**
     * Set timeZoneId
     *
     * @param string $timeZoneId
     * @return BusinessParam
     */
    public function setTimeZoneId($timeZoneId) {
        $this->timeZoneId = $timeZoneId;

        return $this;
    }

    /**
     * Get timeZoneId
     *
     * @return string 
     */
    public function getTimeZoneId() {
        return $this->timeZoneId;
    }

    /**
     * Set ticketShowBusinessIcon
     *
     * @param boolean $ticketShowBusinessIcon
     * @return BusinessParam
     */
    public function setTicketShowBusinessIcon($ticketShowBusinessIcon) {
        $this->ticketShowBusinessIcon = $ticketShowBusinessIcon;

        return $this;
    }

    /**
     * Get ticketShowBusinessIcon
     *
     * @return boolean 
     */
    public function getTicketShowBusinessIcon() {
        return $this->ticketShowBusinessIcon;
    }

    /**
     * Set ticketShowBusinessAddress
     *
     * @param boolean $ticketShowBusinessAddress
     * @return BusinessParam
     */
    public function setTicketShowBusinessAddress($ticketShowBusinessAddress) {
        $this->ticketShowBusinessAddress = $ticketShowBusinessAddress;

        return $this;
    }

    /**
     * Get ticketShowBusinessAddress
     *
     * @return boolean 
     */
    public function getTicketShowBusinessAddress() {
        return $this->ticketShowBusinessAddress;
    }

    /**
     * Set ticketShowBusinessMainPhone
     *
     * @param boolean $ticketShowBusinessMainPhone
     * @return BusinessParam
     */
    public function setTicketShowBusinessMainPhone($ticketShowBusinessMainPhone) {
        $this->ticketShowBusinessMainPhone = $ticketShowBusinessMainPhone;

        return $this;
    }

    /**
     * Get ticketShowBusinessMainPhone
     *
     * @return boolean 
     */
    public function getTicketShowBusinessMainPhone() {
        return $this->ticketShowBusinessMainPhone;
    }

    /**
     * Set ticketShowBusinessContactEmail
     *
     * @param boolean $ticketShowBusinessContactEmail
     * @return BusinessParam
     */
    public function setTicketShowBusinessContactEmail($ticketShowBusinessContactEmail) {
        $this->ticketShowBusinessContactEmail = $ticketShowBusinessContactEmail;

        return $this;
    }

    /**
     * Get ticketShowBusinessContactEmail
     *
     * @return boolean 
     */
    public function getTicketShowBusinessContactEmail() {
        return $this->ticketShowBusinessContactEmail;
    }

    /**
     * Set ticketShowFreeTextHeader
     *
     * @param boolean $ticketShowFreeTextHeader
     * @return BusinessParam
     */
    public function setTicketShowFreeTextHeader($ticketShowFreeTextHeader) {
        $this->ticketShowFreeTextHeader = $ticketShowFreeTextHeader;

        return $this;
    }

    /**
     * Get ticketShowFreeTextHeader
     *
     * @return boolean 
     */
    public function getTicketShowFreeTextHeader() {
        return $this->ticketShowFreeTextHeader;
    }

    /**
     * Set ticketFreeTextHeader
     *
     * @param string $ticketFreeTextHeader
     * @return BusinessParam
     */
    public function setTicketFreeTextHeader($ticketFreeTextHeader) {
        $this->ticketFreeTextHeader = $ticketFreeTextHeader;

        return $this;
    }

    /**
     * Get ticketFreeTextHeader
     *
     * @return string 
     */
    public function getTicketFreeTextHeader() {
        return $this->ticketFreeTextHeader;
    }

    /**
     * Set ticcketShowFreeTextFooter
     *
     * @param boolean $ticcketShowFreeTextFooter
     * @return BusinessParam
     */
    public function setTiccketShowFreeTextFooter($ticcketShowFreeTextFooter) {
        $this->ticcketShowFreeTextFooter = $ticcketShowFreeTextFooter;

        return $this;
    }

    /**
     * Get ticcketShowFreeTextFooter
     *
     * @return boolean 
     */
    public function getTiccketShowFreeTextFooter() {
        return $this->ticcketShowFreeTextFooter;
    }

    /**
     * Set ticketFreeTextFooter
     *
     * @param string $ticketFreeTextFooter
     * @return BusinessParam
     */
    public function setTicketFreeTextFooter($ticketFreeTextFooter) {
        $this->ticketFreeTextFooter = $ticketFreeTextFooter;

        return $this;
    }

    /**
     * Get ticketFreeTextFooter
     *
     * @return string 
     */
    public function getTicketFreeTextFooter() {
        return $this->ticketFreeTextFooter;
    }

    /**
     * Add taxs
     *
     * @param \Sistema\IRMBundle\Entity\Tax $taxs
     * @return BusinessParam
     */
    public function addTax(\Sistema\IRMBundle\Entity\Tax $taxs) {
        $taxs->setBusinessParam($this);
        $this->taxs[] = $taxs;

        return $this;
    }

    /**
     * Remove taxs
     *
     * @param \Sistema\IRMBundle\Entity\Tax $taxs
     */
    public function removeTax(\Sistema\IRMBundle\Entity\Tax $taxs) {
        $this->taxs->removeElement($taxs);
    }

    /**
     * Get taxs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTaxs() {
        return $this->taxs;
    }

    /**
     * Set business
     *
     * @param \Sistema\IRMBundle\Entity\Businesses $business
     * @return BusinessParam
     */
    public function setBusiness(\Sistema\IRMBundle\Entity\Businesses $business = null) {
        $this->business = $business;

        return $this;
    }

    /**
     * Get business
     *
     * @return \Sistema\IRMBundle\Entity\Businesses 
     */
    public function getBusiness() {
        return $this->business;
    }

}
