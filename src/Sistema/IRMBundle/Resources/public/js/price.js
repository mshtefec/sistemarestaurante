// Get the ul that holds the collection
var $collectionPrice;
// setup an "add a tag" link
var $addPriceLink = $('<a href="javascript:;" class="add_price_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newPriceLinkLi = $('<div></div>');

jQuery(document).ready(function () {
    // Get the ul that holds the collection of tags
    $collectionPrice= $('.price');
    // add the "add a tag" anchor and li to the tags ul

    $collectionPrice.append($newPriceLinkLi);
    $collectionPrice.append($addPriceLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionPrice.data('index', $collectionPrice.find(':input').length);
    $("#nuevoPrice").on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPrice, $newPriceLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newPriceLinkLi).offset().top
        }, 1000);
    });
    $addPriceLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPrice, $newPriceLinkLi);
    });
    $collectionPrice.delegate('.delete_price', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});