// Get the ul that holds the collection of escuelas
var collectionOrderItems = jQuery('.orderItems');
var index;

jQuery(document).ready(function() {

    collectionOrderItems.data('index', collectionOrderItems.find(':input').length);

    jQuery('.orderItems').delegate('.btnRemoveOrderItems', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        var li = jQuery(this).closest('.rowremove').attr("li");
        jQuery('#' + li).remove();
        removeForm(jQuery(this).closest('.rowremove'));
    });
    
    jQuery('.ribon_dom').delegate('.add_OrderItems_link', 'click', function(e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        index = addForm(collectionOrderItems, jQuery('.orderItems'));
        if (index === 0) {
            addUl(index, 'Seleccione Order Item');
            jQuery('#orderItems-li-' + index).addClass('active');
            jQuery('#orderItems_' + index).addClass('active');
        } else {
            addUl(index, 'Seleccione Order Item');
        }

    });
});