// Get the ul that holds the collection
var $collectionModifierPrice;
// setup an "add a tag" link
var $addModifierPriceLink = $('<a href="javascript:;" class="add_modifierprice_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newModifierPriceLinkLi = $('<div></div>');

jQuery(document).ready(function () {
    // Get the ul that holds the collection of tags
    $collectionModifierPrice= $('.modifierPrice');
    // add the "add a tag" anchor and li to the tags ul

    $collectionModifierPrice.append($newModifierPriceLinkLi);
    $collectionModifierPrice.append($addModifierPriceLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionModifierPrice.data('index', $collectionModifierPrice.find(':input').length);
    $("#nuevoModifierPrice").on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionModifierPrice, $newModifierPriceLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newModifierPriceLinkLi).offset().top
        }, 1000);
    });
    $addModifierPriceLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionModifierPrice, $newModifierPriceLinkLi);
    });
    $collectionModifierPrice.delegate('.delete_modifierPrice', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});