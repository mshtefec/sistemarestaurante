function mensajePNotify(_title, _text, _type) {
    new PNotify({
        title: _title,
        text: _text,
        type: _type,
        hide: true
    });
}


function controlMenu($menu, $negocio) {
    if ($menu === 0) {
        $("#menu-productos").addClass('disabled');
        $("#menu-productos").attr( "title", "Debe haber un Menu Creado" );
        $("#menu-productos").tooltip();
        $("#menu-productos").click(function (event) {
            event.preventDefault();
        });
        $("#menu-categorias").addClass('disabled');
        $("#menu-categorias").attr( "title", "Debe haber un Menu Creado" );
        $("#menu-categorias").tooltip();
        $("#menu-categorias").click(function (event) {
            event.preventDefault();
        });
    } else {
        $("#menu-productos").removeClass('disabled');
        $("#menu-categorias").removeClass('disabled');
    }
    if ($negocio === 0) {
        $("#menu-entityImages").addClass('disabled');
        $("#menu-entityImages").attr( "title", "Debe haber un Negocio Seleccionado" );
        $("#menu-entityImages").tooltip();
        $("#menu-entityImages").click(function (event) {
            event.preventDefault();
        });
        $("#menu-tables").addClass('disabled');
        $("#menu-tables").attr( "title", "Debe haber un Negocio Seleccionado" );
        $("#menu-tables").tooltip();
        $("#menu-tables").click(function (event) {
            event.preventDefault();
        });
        $("#menu-deliverydates").addClass('disabled');
        $("#menu-deliverydates").attr( "title", "Debe haber un Negocio Seleccionado" );
        $("#menu-deliverydates").tooltip();
        $("#menu-deliverydates").click(function (event) {
            event.preventDefault();
        });
        $("#menu-orders").addClass('disabled');
        $("#menu-orders").attr( "title", "Debe haber un Negocio Seleccionado" );
        $("#menu-orders").tooltip();
        $("#menu-orders").click(function (event) {
            event.preventDefault();
        });
        $("#menu-menues").addClass('disabled');
        $("#menu-menues").attr( "title", "Debe haber un Negocio Seleccionado" );
        $("#menu-menues").tooltip();
        $("#menu-menues").click(function (event) {
            event.preventDefault();
        });
    } else {
        $("#menu-entityImages").removeClass('disabled');
        $("#menu-tables").removeClass('disabled');
        $("#menu-deliverydates").removeClass('disabled');
        $("#menu-orders").removeClass('disabled');
        $("#menu-menues").removeClass('disabled');
    }
}