<?php

namespace Sistema\IRMBundle\EventListener;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class FilterListener {

    /**
     * @var string
     */
    protected $session;

    /**
     * @var string
     */
    protected $em;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(Session $session, EntityManager $em, TokenStorage $tokenStorage) {
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest() {
        if ($this->getUser() != "anon." && !is_null($this->getUser()) && !in_array("ROLE_USER", $this->getUser()->getRoles())) {
            $this->em->getConfiguration()->addFilter('negocio_filter', 'Sistema\IRMBundle\Filter\NegocioFilter');
            $filter = $this->em->getFilters()->enable('negocio_filter');
            $filter->setParameter('negocio', $this->session->get('idNegocio'));
            $filter->setParameter('menu', $this->session->get('idMenu'));
            $filter->setParameter('user', $this->getUser()->getId());
        }
    }

    public function getUser() {
        if (!is_null($this->tokenStorage->getToken())) {
            return $this->tokenStorage->getToken()->getUser();
        }
        return null;
    }

}
