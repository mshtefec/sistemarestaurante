<?php

namespace Sistema\IRMBundle\EventListener;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginListener {

    /**
     * @var string
     */
    protected $idNegocio;

    /**
     * @var string
     */
    protected $idMenu;

    /**
     * @var string
     */
    protected $nombreNegocio;

    /**
     * @var string
     */
    protected $user;

    /**
     * Router
     *
     * @var Router
     */
    protected $router;

    /**
     * EntityManager
     *
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(SecurityContext $securityContext, Router $router, EntityManager $em, Session $session) {
        $this->securityContext = $securityContext;
        $this->router = $router;
        $this->em = $em;
        $this->session = $session;
    }

    public function handle(AuthenticationEvent $event) {
        $token = $event->getAuthenticationToken();
        $this->user = $token->getUser();
        //  ladybug_dump_die($user);
        try {
            if ($this->user != "anon.") {
                $this->session->clear();
                if (!is_null($this->user->getConfiguraciones())) {
                    $this->idNegocio = $this->user->getConfiguraciones()["idNegocio"];
                    $this->idMenu = $this->user->getConfiguraciones()["idMenu"];
                    $this->nombreNegocio = $this->user->getConfiguraciones()["nombreNegocio"];
                } else {
                    $this->idNegocio = 0;
                    $this->idMenu = 0;
                    $this->nombreNegocio = "No se selecciono ningun negocio";
                }
            }
        } catch (Exception $e) {
            
        }
        /* $isString = is_string($token->getUser()->getRoles()[0]);

          if ($isString && $isString == 'ROLE_SUPER_ADMIN') {
          $this->idGimnasio = 0;
          $this->cuotas = 0;
          } else {
          if (!is_null($token->getUser()->getGimnasios())) {
          $this->negocio = $token->getUser()->getGimnasios()[0]->getId();
          }
          $this->cuotas = count($this->em->getRepository('SistemaGymBundle:Asistencia')->findAistenciaSinCuota($this->idGimnasio));
          } */
    }

    public function onKernelResponse(FilterResponseEvent $event) {
        $request = $event->getRequest();
        // ladybug_dump_die($token->getUser()->getConfiguraciones());

        if (!is_null($this->idNegocio)) {
            $request->getSession()->set('idNegocio', $this->idNegocio);
            $request->getSession()->set('idMenu', $this->idMenu);
            $request->getSession()->set('negocioNombre', $this->nombreNegocio);
        }
    }

}
