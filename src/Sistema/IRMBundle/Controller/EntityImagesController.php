<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\EntityImages;
use Sistema\IRMBundle\Form\EntityImagesType;
use Sistema\IRMBundle\Form\EntityImagesFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\File;
use Rhumsaa\Uuid\Uuid;

/**
 * EntityImages controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/entityimages")
 */
class EntityImagesController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/EntityImages.yml',
    );

    /**
     * Lists all EntityImages entities.
     *
     * @Route("/", name="admin_entityimages")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new EntityImagesFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new EntityImages entity.
     *
     * @Route("/", name="admin_entityimages_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:EntityImages:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new EntityImagesType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);
            $entity->setUser($userBusinesses->getUser());


            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            $entity->setSyncprocesstimestamp("12");

            $entity->setUploadDir($entity->getUploadDir() . "/" . $userBusinesses->getUser()->getUuid());
            if (!is_null($entity->getFileSource())) {
                $entity->setTypesincluded("1");
            }

            if (!is_null($entity->getFileCropped())) {
                if (!is_null($entity->getFileSource())) {
                    $entity->setTypesincluded($entity->getTypesincluded() . ",2");
                } else {
                    $entity->setTypesincluded("2");
                }
            }
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new EntityImages entity.
     *
     * @Route("/new", name="admin_entityimages_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new EntityImagesType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a EntityImages entity.
     *
     * @Route("/{id}", name="admin_entityimages_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing EntityImages entity.
     *
     * @Route("/{id}/edit", name="admin_entityimages_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new EntityImagesType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing EntityImages entity.
     *
     * @Route("/{id}", name="admin_entityimages_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:EntityImages:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new EntityImagesType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a EntityImages entity.
     *
     * @Route("/{id}", name="admin_entityimages_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter EntityImages.
     *
     * @Route("/exporter/{format}", name="admin_entityimages_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a EntityImages entity.
     *
     * @Route("/autocomplete-forms/get-owner", name="EntityImages_autocomplete_owner")
     */
    public function getAutocompleteOwners() {
        $options = array(
            'repository' => "SistemaIRMBundle:Owners",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * @Route("/create-entityimages", name="admin_entityimages_post_ajax")
     * @Method("POST")
     */
    public function createEntityImages() {
        $request = $this->getRequest();
        $imagenCropped = $request->request->get('imagenCropped', null);
        $imagenSource = $request->request->get('imagenSource', null);
        $usuario = $request->request->get('usuario', null);
        $em = $this->getDoctrine()->getManager();
        $entity = new EntityImages();
        //usuario del negocio
        if (!$usuario) {
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);
            $entity->setUploadDir($entity->getUploadDir() . "/" . $userBusinesses->getUser()->getUuid());
            $entity->setUser($userBusinesses->getUser());
        } else {
            $user = $em->getRepository('SistemaUserBundle:User')->find($usuario);
            $entity->setUploadDir($entity->getUploadDir() . "/" . $user->getUuid());
            $entity->setUser($user);
        }




        if (!empty($imagenSource)) {
            $pos = strpos($imagenSource, ',');
            $img = substr($imagenSource, $pos + 1);
            $data = base64_decode($img);
            $entity->setFileSource($data);
            $entity->setTypesincluded("1");
        }
        if (!empty($imagenCropped)) {
            $pos = strpos($imagenCropped, ',');
            $img = substr($imagenCropped, $pos + 1);
            $data = base64_decode($img);

            $entity->setFileCropped($data);
            if (!empty($imagenSource)) {
                $entity->setTypesincluded("2");
            } else {
                $entity->setTypesincluded($entity->getTypesincluded() . ",2");
            }
        }



        $resultado = false;

        //genero uuid
        $uuid = Uuid::uuid1();
        $uuid = $uuid->getFieldsHex();
        $uuid = $uuid["time_low"] . "-" .
                $uuid["time_mid"] . "-" .
                $uuid["time_hi_and_version"] . "-" .
                $uuid["clock_seq_hi_and_reserved"] .
                $uuid["clock_seq_low"] . "-" .
                $uuid["node"];
        $entity->setUuid($uuid);

        $entity->setSyncprocesstimestamp("12");
        $em->persist($entity);
        $em->flush();
        $resultado = true;


        $array[] = array(
            'resultado' => $resultado,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/get-entityimages", name="admin_entityimages_getById_ajax")
     */
    public function getEntityImageByIdAjax() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $term = $request->request->get('entityImage', null);
        $entity = $em->getRepository($config['repository'])->find($term);


        $array[] = array(
            'source' => $entity->getWebPathSource(),
            'cropped' => $entity->getWebPathCropped()
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     *
     * @Route("/get-all-entityimages", name="admin_entityimages_getallEntityimages_ajax")
     */
    public function getEntityImageAllAjax() {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        //$entitys = $em->getRepository($config['repository'])->findAll();
        $request = $this->getRequest();
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $usuario = $request->request->get('usuario', null);
        $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);

      
        if (is_null($usuario)) {
            if (is_null($userBusinesses)) {
                $entitys = $em->getRepository('SistemaIRMBundle:EntityImages')->getEntityImagesByUserBusinessLogin($this->getUser()->getId());
            } else {
                $entitys = $em->getRepository('SistemaIRMBundle:EntityImages')->getEntityImagesByUserBusinessLogin($this->getUser()->getId(), $userBusinesses->getUser()->getId());
            }
        } else {
            $entitys = $em->getRepository('SistemaIRMBundle:EntityImages')->getEntityImagesByUserBusinessLogin($this->getUser()->getId(), $usuario);
        }


        $array = array();

        foreach ($entitys as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'pathSource' => $entity->getWebPathSource(),
                'pathCropped' => $entity->getWebPathCropped()
            );
        }
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);

        $queryBuilder = $em->getRepository($repository)
                ->createQueryBuilder('a')
                ->where('a.user = :user1 or a.user= :user2')
                ->setParameter('user1', $this->getUser()->getId())
                ->setParameter('user2', $userBusinesses->getUser()->getId())
                ->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }

}
