<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\DeliveryDates;
use Sistema\IRMBundle\Form\DeliveryDatesType;
use Sistema\IRMBundle\Form\DeliveryDatesFilterType;

/**
 * DeliveryDates controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/deliverydates")
 */
class DeliveryDatesController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/DeliveryDates.yml',
    );

    /**
     * Lists all DeliveryDates entities.
     *
     * @Route("/", name="admin_deliverydates")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new DeliveryDatesFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new DeliveryDates entity.
     *
     * @Route("/", name="admin_deliverydates_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:DeliveryDates:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new DeliveryDatesType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new DeliveryDates entity.
     *
     * @Route("/new", name="admin_deliverydates_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new DeliveryDatesType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a DeliveryDates entity.
     *
     * @Route("/{id}", name="admin_deliverydates_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing DeliveryDates entity.
     *
     * @Route("/{id}/edit", name="admin_deliverydates_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new DeliveryDatesType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing DeliveryDates entity.
     *
     * @Route("/{id}", name="admin_deliverydates_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:DeliveryDates:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new DeliveryDatesType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a DeliveryDates entity.
     *
     * @Route("/{id}", name="admin_deliverydates_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter DeliveryDates.
     *
     * @Route("/exporter/{format}", name="admin_deliverydates_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }
}