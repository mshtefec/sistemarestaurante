<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Menus;
use Sistema\IRMBundle\Form\MenusType;
use Sistema\IRMBundle\Form\MenusFilterType;
use Sistema\IRMBundle\Entity\Folders;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Menus controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/menus")
 */
class MenusController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Menus.yml',
    );

    /**
     * Lists all Menus entities.
     *
     * @Route("/", name="admin_menus")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('SistemaIRMBundle:Menus')->getMenusByUser($this->getUser()->getId());
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );

        unset($config['filterType']);
        return array(
            'config' => $config,
            'entities' => $pagination,
        );
    }

    /**
     * Creates a new Menus entity.
     *
     * @Route("/", name="admin_menus_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Menus:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new MenusType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //creo folder root
            $folderRoot = new Folders();
            $folderRoot->setMenu($entity);
            $folderRoot->setLvl(0);
            $folderRoot->setDescription("root");
            $folderRoot->setName("root");
            $folderRoot->setIsdeleted(0);
            //genero uuid para folder root
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $folderRoot->setUuid($uuid);
            $folderRoot->setVisible(1);
            $folderRoot->setAllowordering(1);
            $folderRoot->setStatus(1);
            $folderRoot->setIsdeleted(1);
            $entity->addFolder($folderRoot);
            
            //genero uuid para menu
            $uuid2 = Uuid::uuid1();
            $uuid2 = $uuid2->getFieldsHex();
            $uuid2 = $uuid2["time_low"] . "-" .
                    $uuid2["time_mid"] . "-" .
                    $uuid2["time_hi_and_version"] . "-" .
                    $uuid2["clock_seq_hi_and_reserved"] .
                    $uuid2["clock_seq_low"] . "-" .
                    $uuid2["node"];
            $entity->setUuid($uuid2);
            $entity->setUser($this->getUser());
            $deleted = $request->request->get('deleted', null);

            //seteo deleted
            if ($deleted) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            $em = $this->getDoctrine()->getManager();
            //pregunto si tiene en session negocio sino el el dueño del menu es del primer negocio
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');

            if (!is_null($idNegocio) && $idNegocio != 0) {
                $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);
                $entity->addBusiness($userBusinesses->getBusiness());
            } else {
                $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($entity->getBusinesses()->first()->getId());
            }
            $entity->setUser($userBusinesses->getUser());

            $em->persist($entity);
            $em->flush();
            $session->set('idMenu', $userBusinesses->getBusiness()->getMenu()->getId());
            $user = $this->getUser();
            $user->setItemConfiguraciones("idMenu", $userBusinesses->getBusiness()->getMenu()->getId());
            $em->persist($user);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Menus entity.
     *
     * @Route("/new", name="admin_menus_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new MenusType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Menus entity.
     *
     * @Route("/{id}", name="admin_menus_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Menus entity.
     *
     * @Route("/{id}/edit", name="admin_menus_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new MenusType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Menus entity.
     *
     * @Route("/{id}", name="admin_menus_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Menus:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new MenusType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $isDeleted = $editForm->get('deleted')->getData();

            //seteo deleted
            if ($isDeleted) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Menus entity.
     *
     * @Route("/{id}", name="admin_menus_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Menus.
     *
     * @Route("/exporter/{format}", name="admin_menus_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-owner", name="Menus_autocomplete_owner")
     */
    public function getAutocompleteOwners() {
        $options = array(
            'repository' => "SistemaIRMBundle:Owners",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-businesses", name="Menus_autocomplete_businesses")
     */
    public function getAutocompleteBusinesses() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-businesses-by-user", name="Menus_autocomplete_businesses_by_user")
     */
    public function getAutocompleteBusinessesByUser() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );
        
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($options['repository'])->getAllBusinessesByuser($this->getUser()->getId());

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getName(),
            );
        }
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-folders", name="Menus_autocomplete_folders")
     */
    public function getAutocompleteFolders() {
        $options = array(
            'repository' => "SistemaIRMBundle:Folders",
            'field' => "id",
        );

        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($options['repository'])->getAllFoldersByuser($this->getUser()->getId());

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getName(),
            );
        }
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-products", name="Menus_autocomplete_products")
     */
    public function getAutocompleteProducts() {
        $options = array(
            'repository' => "SistemaIRMBundle:Products",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

}
