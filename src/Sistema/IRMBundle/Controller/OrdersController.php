<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Orders;
use Sistema\IRMBundle\Form\OrdersType;
use Sistema\IRMBundle\Form\OrdersFilterType;
use Rhumsaa\Uuid\Uuid;

/**
 * Orders controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/orders")
 */
class OrdersController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Orders.yml',
    );

    /**
     * Lists all Orders entities.
     *
     * @Route("/", name="admin_orders")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new OrdersFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Orders entity.
     *
     * @Route("/", name="admin_orders_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Orders:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new OrdersType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            //genero uuid para orderitems
            foreach ($entity->getOrderItems() as $orderItems) {
                //genero uuid
                $uuid = Uuid::uuid1();
                $uuid = $uuid->getFieldsHex();
                $uuid = $uuid["time_low"] . "-" .
                        $uuid["time_mid"] . "-" .
                        $uuid["time_hi_and_version"] . "-" .
                        $uuid["clock_seq_hi_and_reserved"] .
                        $uuid["clock_seq_low"] . "-" .
                        $uuid["node"];
                $orderItems->setUuid($uuid);
            }


            $isDeleted = $request->request->get('order_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            //seteo usuario y negocio
            //seteo negocio
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $userBusiness = $em->getRepository('SistemaIRMBundle:UserBusinesses')->getUserRoleNegocioForBusiness($idNegocio);
            $entity->setUser($userBusiness->getUser());
            $entity->setBusiness($userBusiness->getBusiness());
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Orders entity.
     *
     * @Route("/new", name="admin_orders_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new OrdersType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Orders entity.
     *
     * @Route("/{id}", name="admin_orders_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Orders entity.
     *
     * @Route("/{id}/edit", name="admin_orders_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new OrdersType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Orders entity.
     *
     * @Route("/{id}", name="admin_orders_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Orders:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new OrdersType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $isDeleted = $request->request->get('order_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }


            //genero uuid para orderitems
            foreach ($entity->getOrderItems() as $orderItems) {
                if (is_null($orderItems->getUuid())) {
                    //genero uuid
                    $uuid = Uuid::uuid1();
                    $uuid = $uuid->getFieldsHex();
                    $uuid = $uuid["time_low"] . "-" .
                            $uuid["time_mid"] . "-" .
                            $uuid["time_hi_and_version"] . "-" .
                            $uuid["clock_seq_hi_and_reserved"] .
                            $uuid["clock_seq_low"] . "-" .
                            $uuid["node"];
                    $orderItems->setUuid($uuid);
                }
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Orders entity.
     *
     * @Route("/{id}", name="admin_orders_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Orders.
     *
     * @Route("/exporter/{format}", name="admin_orders_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Orders entity.
     *
     * @Route("/autocomplete-forms/get-user", name="Orders_autocomplete_user")
     */
    public function getAutocompleteUser() {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Orders entity.
     *
     * @Route("/autocomplete-forms/get-business", name="Orders_autocomplete_business")
     */
    public function getAutocompleteBusinesses() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Orders entity.
     *
     * @Route("/autocomplete-forms/get-table", name="Orders_autocomplete_table")
     */
    public function getAutocompleteTables() {
        $options = array(
            'repository' => "SistemaIRMBundle:Tables",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Orders entity.
     *
     * @Route("/autocomplete-forms/get-orderItems", name="Orders_autocomplete_orderItems")
     */
    public function getAutocompleteOrderItems() {
        $options = array(
            'repository' => "SistemaIRMBundle:OrderItems",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a DeliveryDates entity.
     *
     * @Route("/autocomplete-forms/get-deliveryDates", name="Orders_autocomplete_deliverydate")
     */
    public function getAutocompleteDeliveryDates() {
        $options = array(
            'repository' => "SistemaIRMBundle:DeliveryDates",
            'field' => "deliveryDates",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

}
