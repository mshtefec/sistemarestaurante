<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Products;
use Sistema\IRMBundle\Form\ProductsType;
use Sistema\IRMBundle\Form\ProductsFilterType;
use Symfony\Component\Yaml\Yaml;
use Hateoas\HateoasBuilder;
use Sistema\IRMBundle\Form\EntityImagesType;
use Sistema\IRMBundle\Entity\EntityImages;
use Symfony\Component\HttpFoundation\JsonResponse;
use Rhumsaa\Uuid\Uuid;

/**
 * Products controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/products")
 */
class ProductsController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Products.yml',
    );

    /**
     * Lists all Products entities.
     *
     * @Route("/", name="admin_products")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('SistemaIRMBundle:Products')->getProductsByMenu($session->get('idMenu'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );

        unset($config['filterType']);
        return array(
            'config' => $config,
            'entities' => $pagination,
        );
    }

    /**
     * Creates a new Products entity.
     *
     * @Route("/", name="admin_products_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Products:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ProductsType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);
        if ($form->isValid()) {
            //transformar en json entity y codifico en base 64
            $hateoas = HateoasBuilder::create()->build();
            $arrayPriceJson = $hateoas->serialize($entity->getPrices(), 'json');
            $pricearrayserialized = base64_encode($arrayPriceJson);

            $arrayModifierPriceJson = $hateoas->serialize($entity->getModifierPrices(), 'json');
            $modifierpricearrayserialized = base64_encode($arrayModifierPriceJson);

            //guardo en la entidad
            $entity->setPricearrayserialized($pricearrayserialized);
            $entity->setModifierpricearrayserialized($modifierpricearrayserialized);


            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            $isAllowOrdening = $form->get('allowOrdening')->getData();
            $isVisible = $form->get('visible')->getData();

            //seteo visible
            if ($isVisible) {
                $entity->setVisible("1");
            } else {
                $entity->setVisible("0");
            }

            //seteo allowOrdening
            if ($isAllowOrdening) {
                $entity->setAllowordering("1");
            } else {
                $entity->setAllowordering("0");
            }
            //seteo entityImagen
            $em = $this->getDoctrine()->getManager();

            $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
            if (!is_null($entityImage)) {
                $entity->setImage($entityImage);
            }
            //seteo el menu
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $business = $em->getRepository('SistemaIRMBundle:Businesses')->find($idNegocio);
            $entity->setMenu($business->getMenu());


            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');


        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Displays a form to create a new Products entity.
     *
     * @Route("/new", name="admin_products_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ProductsType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');

        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Finds and displays a Products entity.
     *
     * @Route("/{id}", name="admin_products_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {

        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Products entity.
     *
     * @Route("/{id}/edit", name="admin_products_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ProductsType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        $formEntityImages = $this->createForm(new EntityImagesType(), $entityImages, array(
            'action' => $this->generateUrl($configEntityImages['create']),
            'method' => 'POST',
        ));

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Edits an existing Products entity.
     *
     * @Route("/{id}", name="admin_products_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Products:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ProductsType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $resgImagen = null;
        if (!is_null($entity->getImage())) {
            $resgImagen = $entity->getImage()->getId();
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);
        if ($editForm->isValid()) {
            //transformar en json entity y codifico en base 64
            $hateoas = HateoasBuilder::create()->build();
            $arrayPriceJson = $hateoas->serialize($entity->getPrices(), 'json');
            $pricearrayserialized = base64_encode($arrayPriceJson);

            $arrayModifierPriceJson = $hateoas->serialize($entity->getModifierPrices(), 'json');
            $modifierpricearrayserialized = base64_encode($arrayModifierPriceJson);

            //guardo en la entidad
            $entity->setPricearrayserialized($pricearrayserialized);
            $entity->setModifierpricearrayserialized($modifierpricearrayserialized);



            $isAllowOrdening = $editForm->get('allowOrdening')->getData();
            $isVisible = $editForm->get('visible')->getData();
            $isDeleted = $editForm->get('deleted')->getData();

            //seteo visible
            if ($isVisible) {
                $entity->setVisible("1");
            } else {
                $entity->setVisible("0");
            }

            //seteo allowOrdening
            if ($isAllowOrdening) {
                $entity->setAllowordering("1");
            } else {
                $entity->setAllowordering("0");
            }

            //seteo deleted
            if ($isDeleted) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
            if ((!is_null($entityImage)) && ($idEntityImnage != $resgImagen)) {
                $entity->setImage($entityImage);
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        $formEntityImages = $this->createForm(new EntityImagesType(), $entityImages, array(
            'action' => $this->generateUrl($configEntityImages['create']),
            'method' => 'POST',
        ));

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Deletes a Products entity.
     *
     * @Route("/{id}", name="admin_products_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Products.
     *
     * @Route("/exporter/{format}", name="admin_products_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-menu", name="Products_autocomplete_menu")
     */
    public function getAutocompleteMenus() {
        $options = array(
            'repository' => "SistemaIRMBundle:Menus",
            'field' => "name",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb
                ->select('a')
                ->from($options['repository'], 'a')
                ->leftJoin('a.businesses', 'b')
                ->Where("a." . $options['field'] . " LIKE ?1 and b.id= :bus")
                ->setParameter(1, "%" . $term . "%")
                ->setParameter('bus', $idNegocio)
                ->getQuery()
                ->getResult()
        ;

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-image", name="Products_autocomplete_image")
     */
    public function getAutocompleteEntityImages() {
        $options = array(
            'repository' => "SistemaIRMBundle:EntityImages",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-folder", name="Products_autocomplete_folder")
     */
    public function getAutocompleteFolders() {
        $options = array(
            'repository' => "SistemaIRMBundle:Folders",
            'field' => "name",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb
                ->select('a')
                ->from($options['repository'], 'a')
                ->leftJoin('a.menu', 'm')
                ->leftJoin('m.businesses', 'b')
                ->Where("a." . $options['field'] . " LIKE ?1 and b.id= :bus")
                ->setParameter(1, "%" . $term . "%")
                ->setParameter('bus', $idNegocio)
                ->getQuery()
                ->getResult()
        ;

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-orderItems", name="Products_autocomplete_orderItems")
     */
    public function getAutocompleteOrderItems() {
        $options = array(
            'repository' => "SistemaIRMBundle:OrderItems",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-businesses", name="Products_autocomplete_businesses")
     */
    public function getAutocompleteBusinesses() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Products entity.
     *
     * @Route("/autocomplete-forms/get-orders-item", name="Products_autocomplete_orders")
     */
    public function getAutocompleteOrders() {
        $options = array(
            'repository' => "SistemaIRMBundle:OrderItems",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    private function crearFormulario($formType, $entity, $ruta, $id = "form") {
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($ruta),
            'method' => 'POST',
            'attr' => array('id' => $id)
        ));

        return $form;
    }

}
