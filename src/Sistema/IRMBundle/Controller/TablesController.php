<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Tables;
use Sistema\IRMBundle\Form\TablesType;
use Sistema\IRMBundle\Form\TablesFilterType;
use Symfony\Component\Yaml\Yaml;
use Sistema\IRMBundle\Form\EntityImagesType;
use Sistema\IRMBundle\Entity\EntityImages;
use Rhumsaa\Uuid\Uuid;

/**
 * Tables controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/tables")
 */
class TablesController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Tables.yml',
    );

    /**
     * Lists all Tables entities.
     *
     * @Route("/", name="admin_tables")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new TablesFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Tables entity.
     *
     * @Route("/", name="admin_tables_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Tables:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new TablesType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            $isDeleted = $request->request->get('tables_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            //seteo entityImagen
            if (!is_null($idEntityImnage)) {
                $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
                if (!is_null($entityImage)) {
                    $entity->setImage($entityImage);
                }
            }

            //seteo negocio
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $negocio = $em->getRepository('SistemaIRMBundle:Businesses')->find($idNegocio);
            if (!is_null($negocio)) {
                $entity->setBusiness($negocio);
                $em->persist($entity);
                $em->flush();
                $this->useACL($entity, 'create');

                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Tables entity.
     *
     * @Route("/new", name="admin_tables_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new TablesType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Finds and displays a Tables entity.
     *
     * @Route("/{id}", name="admin_tables_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Tables entity.
     *
     * @Route("/{id}/edit", name="admin_tables_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new TablesType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Edits an existing Tables entity.
     *
     * @Route("/{id}", name="admin_tables_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Tables:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new TablesType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        if (!is_null($entity->getImage())) {
            $resgImagen = $entity->getImage()->getId();
        }

        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);

        if ($editForm->isValid()) {
            if (!is_null($idEntityImnage)) {
                $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
                if ((!is_null($entityImage)) && ($idEntityImnage != $resgImagen)) {
                    $entity->setImage($entityImage);
                }
            }
            $isDeleted = $request->request->get('tables_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Tables entity.
     *
     * @Route("/{id}", name="admin_tables_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Tables.
     *
     * @Route("/exporter/{format}", name="admin_tables_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Tables entity.
     *
     * @Route("/autocomplete-forms/get-image", name="Tables_autocomplete_image")
     */
    public function getAutocompleteEntityImages() {
        $options = array(
            'repository' => "SistemaIRMBundle:EntityImages",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Tables entity.
     *
     * @Route("/autocomplete-forms/get-business", name="Tables_autocomplete_business")
     */
    public function getAutocompleteBusinesses() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    private function crearFormulario($formType, $entity, $ruta, $id = "form") {
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($ruta),
            'method' => 'POST',
            'attr' => array('id' => $id)
        ));

        return $form;
    }

}
