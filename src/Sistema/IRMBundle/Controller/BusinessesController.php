<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Businesses;
use Sistema\IRMBundle\Form\BusinessesType;
use Sistema\IRMBundle\Form\BusinessesFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\IRMBundle\Entity\UserBusinesses;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\Yaml\Yaml;
use Sistema\IRMBundle\Form\EntityImagesType;
use Sistema\IRMBundle\Entity\EntityImages;
use Hateoas\HateoasBuilder;

/**
 * Businesses controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/businesses")
 */
class BusinessesController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Businesses.yml',
    );

    /**
     * Lists all Businesses entities.
     *
     * @Route("/", name="admin_businesses")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new BusinessesFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Businesses entity.
     *
     * @Route("/", name="admin_businesses_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Businesses:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new BusinessesType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //seteo la imagen
            $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
            if (!is_null($entityImage)) {
                $entity->setBgImage($entityImage);
            }
            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            $isDeleted = $form->get('deleted')->getData();
            $isPublic = $form->get('publico')->getData();

            //seteo is deleted
            if ($isDeleted) {
                $entity->setIsdeleted(1);
            } else {
                $entity->setIsdeleted(0);
            }
            //seteo is public
            if ($isPublic) {
                $entity->setIspublic(1);
            } else {
                $entity->setIspublic(0);
            }

            //tax
            //transformar en json entity y codifico en base 64
            if (count($entity->getBusinessParam()->getTaxs()) > 0) {
                $hateoas = HateoasBuilder::create()->build();
                $arrayTaxsJson = $hateoas->serialize($entity->getBusinessParam()->getTaxs(), 'json');
                $TaxArrayserialized = base64_encode($arrayTaxsJson);
                $entity->getBusinessParam()->setTaxArraySerialized($TaxArrayserialized);
            } else {
                $entity->getBusinessParam()->setTaxArraySerialized("");
            }
            //parametros
            //transformar en json entity y codifico en base 64
            $hateoas = HateoasBuilder::create()->build();
            $arrayBusinessParamJson = $hateoas->serialize($entity->getBusinessParam(), 'json');
            $businessParamarrayserialized = base64_encode($arrayBusinessParamJson);
            $entity->setBusinessparamsserialized($businessParamarrayserialized);

            $entity->setSyncProcessTimeStamp(1);
            $role = current($em->getRepository('SistemaUserBundle:Role')->findByName('ROLE_NEGOCIO'));
            $userBusinesses = new UserBusinesses();
            $userBusinesses->setBusiness($entity);
            $userBusinesses->setUser($this->getUser());
            $userBusinesses->setRole($role);
            $entity->addUserBusiness($userBusinesses);
            $em->persist($entity);
            $em->flush();
            //pregunto si es mi primer negocio y lo pongo en session
            /* $session = $request->getSession();
              $idNegocio = $session->get('idNegocio');
              if (is_null($idNegocio)) {
              $this->modificarValoresSession($entity);
              } */
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Businesses entity.
     *
     * @Route("/new", name="admin_businesses_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new BusinessesType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');
        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Finds and displays a Businesses entity.
     *
     * @Route("/{id}", name="admin_businesses_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Businesses entity.
     *
     * @Route("/{id}/edit", name="admin_businesses_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new BusinessesType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->findbyidjoinuserbussiness($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        foreach ($entity->getUserBusinesses() as $ub) {
            if (!is_null($ub->getRole()) && $ub->getRole()->getName() == "ROLE_NEGOCIO") {
                $usuario = $ub->getUser()->getId();
            }
        }
        
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);
        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        $formEntityImages = $this->createForm(new EntityImagesType(), $entityImages, array(
            'action' => $this->generateUrl($configEntityImages['create']),
            'method' => 'POST',
        ));

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
            'usuario' => $usuario,
        );
    }

    /**
     * Edits an existing Businesses entity.
     *
     * @Route("/{id}", name="admin_businesses_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Businesses:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new BusinessesType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $resgImagen = null;
        if (!is_null($entity->getBgImage())) {
            $resgImagen = $entity->getBgImage()->getId();
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);
        if ($editForm->isValid()) {
            //seteo la imagen
            $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
            if ((!is_null($entityImage)) && ($idEntityImnage != $resgImagen)) {
                $entity->setBgImage($entityImage);
            }

            $isDeleted = $editForm->get('deleted')->getData();
            $isPublic = $editForm->get('publico')->getData();

            //seteo is deleted
            if ($isDeleted) {
                $entity->setIsdeleted(1);
            } else {
                $entity->setIsdeleted(0);
            }
            //seteo is public
            if ($isPublic) {
                $entity->setIspublic(1);
            } else {
                $entity->setIspublic(0);
            }
            //tax
            //transformar en json entity y codifico en base 64
            if (count($entity->getBusinessParam()->getTaxs()) > 0) {
                $hateoas = HateoasBuilder::create()->build();
                $arrayTaxsJson = $hateoas->serialize($entity->getBusinessParam()->getTaxs(), 'json');
                $TaxArrayserialized = base64_encode($arrayTaxsJson);
                $entity->getBusinessParam()->setTaxArraySerialized($TaxArrayserialized);
            } else {
                $entity->getBusinessParam()->setTaxArraySerialized("");
            }
            //parametros
            //transformar en json entity y codifico en base 64
            $hateoas = HateoasBuilder::create()->build();
            $arrayBusinessParamJson = $hateoas->serialize($entity->getBusinessParam(), 'json');
            $businessParamarrayserialized = base64_encode($arrayBusinessParamJson);
            $entity->setBusinessparamsserialized($businessParamarrayserialized);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->createForm(new EntityImagesType(), $entityImages, array(
            'action' => $this->generateUrl($configEntityImages['create']),
            'method' => 'POST',
        ));
        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Deletes a Businesses entity.
     *
     * @Route("/{id}", name="admin_businesses_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Businesses.
     *
     * @Route("/exporter/{format}", name="admin_businesses_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-iconImage", name="Businesses_autocomplete_iconImage")
     */
    public function getAutocompleteEntityImagesIconImage() {
        $options = array(
            'repository' => "SistemaIRMBundle:EntityImages",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-bgImage", name="Businesses_autocomplete_bgImage")
     */
    public function getAutocompleteEntityImages() {
        $options = array(
            'repository' => "SistemaIRMBundle:EntityImages",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-owner", name="Businesses_autocomplete_owner")
     */
    public function getAutocompleteOwners() {
        $options = array(
            'repository' => "SistemaIRMBundle:Owners",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-menu", name="Businesses_autocomplete_menu")
     */
    public function getAutocompleteMenus() {
        $options = array(
            'repository' => "SistemaIRMBundle:Menus",
            'field' => "name",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')->from($options['repository'], 'a')->leftJoin('a.businesses', 'b')->Where("a." . $options['field'] . " LIKE ?1")->setParameter(1, "%" . $term . "%")->getQuery()->getResult()
        ;

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-tables", name="Businesses_autocomplete_tables")
     */
    public function getAutocompleteTables() {
        $options = array(
            'repository' => "SistemaIRMBundle:Tables",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/get-orderItems", name="Businesses_autocomplete_orderItems")
     */
    public function getAutocompleteOrderItems() {
        $options = array(
            'repository' => "SistemaIRMBundle:OrderItems",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/autocomplete-forms/select-businesses", name="Businesses_autocomplete_select")
     */
    public function getAutocompleteBusinesses() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
        $qb->add('where', "(a.name LIKE ?1 or a.description LIKE ?1) and a.user = :usuario ")->add('orderBy', "a.name ASC")->setParameter(1, "%" . $term . "%")->setParameter('usuario', $user)
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/change-businesses/select-businesses", name="Businesses_session_change")
     */
    public function changeBusinessesSession() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $negocio = (int) $request->request->get('negocio', null);
        $entity = $em->getRepository("SistemaIRMBundle:Businesses")->find($negocio);
        $resultado = false;
        try {
            $this->modificarValoresSession($entity);
            $resultado = true;
        } catch (Exception $e) {
            $resultado = false;
        }
        $array = array();
        $array[] = array(
            'resultado' => $resultado,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    private function modificarValoresSession($negocio) {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $user = $this->get('security.context')->getToken()->getUser();
        if (!is_null($negocio)) {
            $user->setItemConfiguraciones("idNegocio", $negocio->getId());
            $user->setItemConfiguraciones("nombreNegocio", $negocio->getName());
        } else {
            $user->setItemConfiguraciones("idNegocio", 0);
            $user->setItemConfiguraciones("nombreNegocio", "");
        }
        if (!is_null($negocio->getMenu())) {
            $user->setItemConfiguraciones("idMenu", $negocio->getMenu()->getId());
        } else {
            $user->setItemConfiguraciones("idMenu", 0);
        }


        $em->flush();
        $session = $request->getSession();
        if (!is_null($negocio)) {
            $session->set('idNegocio', $negocio->getId());
            $session->set('negocioNombre', $negocio->getName());
        } else {
            $session->set('idNegocio', 0);
            $session->set('negocioNombre', "");
        }
        if (!is_null($negocio->getMenu())) {
            $session->set('idMenu', $negocio->getMenu()->getId());
        } else {
            $session->set('idMenu', 0);
        }
    }

    /**
     * Lists all Businesses entities.
     *
     * @Route("/asignar-usuario/{id}", name="admin_businesses_asignar_usuario", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function asignarUsuarioAction($id) {
        $config = $this->getConfig();
        //obtener todos los user del negocio
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository($config['repository'])->getAllUserByBusinesses($id, $this->getUser()->getId());
        $array = array();
        $request = $this->getRequest();
        $session = $request->getSession();

        $session->set('asignarNegocioaUsuario', (int) $id);
        foreach ($users as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getUser()->getEmail(),
            );
        }
        return array(
            'config' => $config,
            'users' => $array,
        );
    }

    /**
     * Lists all Businesses entities.
     *
     * @Route("/asignar-usuario/actualizar/", name="admin_businesses_asignar_usuario_actualizar")
     * @Method("Post")
     */
    public function asignarUsuarioActualizarAction() {
        //obtener todos los user del negocio
        $config = $this->getConfig();
        $request = $this->getRequest();

        $idUser = $request->request->get('user', null);
        $idUserBusiness = $request->request->get('userBusiness', null);
        $persistir = (bool) $request->request->get('persist', null);

        $session = $request->getSession();
        $idNegocio = $session->get('asignarNegocioaUsuario');
        $resultado = false;

        try {
            $em = $this->getDoctrine()->getManager();

            if ($persistir) {
                $user = $em->getRepository('SistemaUserBundle:User')->find($idUser);
                $businesses = $em->getRepository('SistemaIRMBundle:Businesses')->find($idNegocio);
                //$role = current($em->getRepository('SistemaUserBundle:Role')->findByName("ROLE_NEGOCIO"));

                if (!is_null($user) && !is_null($businesses)) {
                    $userBusiness = new UserBusinesses();
                    $userBusiness->setBusiness($businesses);
                    //  $userBusiness->setRole($role);
                    $user->addUserBusiness($userBusiness);
                    $em->persist($user);
                }
            } else {
                $userBusinesses = $em->getRepository('SistemaIRMBundle:UserBusinesses')->find($idUserBusiness);
                if (!is_null($userBusinesses)) {
                    $em->remove($userBusinesses);
                }
            }

            $em->flush();
            $arrayUser = array();
            $users = $em->getRepository($config['repository'])->getAllUserByBusinesses($idNegocio, $this->getUser()->getId());

            foreach ($users as $entity) {
                $arrayUser[] = array(
                    'id' => $entity->getId(),
                    'text' => $entity->getUser()->getEmail(),
                );
            }
            $resultado = true;
        } catch (Exception $e) {
            $resultado = false;
        }
        $array = array();
        $array[] = array(
            'resultado' => $resultado,
            'user' => $arrayUser,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Businesses entity.
     *
     * @Route("/businesses/get-all-byuser", name="admin_Businesses_byUser")
     */
    public function getAllBusinesessByUserAjax() {
        $options = array(
            'repository' => "SistemaIRMBundle:Businesses",
            'field' => "id",
        );

        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $entities = $em->getRepository($options['repository'])->getAllBusinessesByuser($this->getUser()->getId());

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getName(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)->createQueryBuilder('a')->join('a.userBusinesses', 'ub')->where('ub.user= :user')->setParameter('user', $this->getUser()->getId())->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }

    private function crearFormulario($formType, $entity, $ruta, $id = "form") {
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($ruta),
            'method' => 'POST',
            'attr' => array('id' => $id)
        ));

        return $form;
    }

}
