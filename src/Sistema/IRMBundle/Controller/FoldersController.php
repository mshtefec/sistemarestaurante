<?php

namespace Sistema\IRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Folders;
use Sistema\IRMBundle\Form\FoldersType;
use Sistema\IRMBundle\Form\FoldersFilterType;
use Sistema\IRMBundle\Form\EntityImagesType;
use Sistema\IRMBundle\Entity\EntityImages;
use Symfony\Component\Yaml\Yaml;
use Hateoas\HateoasBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Rhumsaa\Uuid\Uuid;

/**
 * Folders controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/folders")
 */
class FoldersController extends Controller {

    private $em;
    private $menu;

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/IRMBundle/Resources/config/Folders.yml',
    );

    /**
     * Lists all Folders entities.
     *
     * @Route("/", name="admin_folders")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Lists all Folders entities.
     *
     * @Route("/byMenu/{id}", name="admin_folders_by_menu")
     * @Method("GET")
     * @Template("SistemaIRMBundle:Folders:index.html.twig")
     */
    public function indexByMenuAction($id) {
        $this->config['filterType'] = new FoldersFilterType();
        $config = $this->getConfig();
        list($filterForm, $queryBuilder) = $this->filter($config);
        $request = $this->getRequest();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );

        //remove the form to return to the view
        unset($config['filterType']);
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $idMenu = $session->get('idMenu');
        
        $repo = $em->getRepository('SistemaIRMBundle:Folders');
        $query = $repo->getFolderArrayByMenu($idMenu);
        
        $arrayTree = $repo->buildTreeArray($query);
        
        $hateoas = HateoasBuilder::create()->build();
        $folderTree = $hateoas->serialize($arrayTree, 'json');
        $folderTree = str_replace("name", "text", $folderTree);
        $folderTree = str_replace("__children", "children", $folderTree);
        
        return array(
            'config' => $config,
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
            'jstree' => $folderTree,
            'idMenu' => $id,
        );
    }

    /**
     * Creates a new Folders entity.
     *
     * @Route("/", name="admin_folders_create")
     * @Method("POST")
     * @Template("SistemaIRMBundle:Folders:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new FoldersType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);

        if ($form->isValid()) {
            //genero uuid
            $uuid = Uuid::uuid1();
            $uuid = $uuid->getFieldsHex();
            $uuid = $uuid["time_low"] . "-" .
                    $uuid["time_mid"] . "-" .
                    $uuid["time_hi_and_version"] . "-" .
                    $uuid["clock_seq_hi_and_reserved"] .
                    $uuid["clock_seq_low"] . "-" .
                    $uuid["node"];
            $entity->setUuid($uuid);
            $visible = $request->request->get('folder_visible', null);
            $allowordering = $request->request->get('folder_allowordering', null);

            //seteo visible
            if (!is_null($visible)) {
                $entity->setVisible("1");
            } else {
                $entity->setVisible("0");
            }

            //seteo allowordering
            if (!is_null($allowordering)) {
                $entity->setAllowordering("1");
            } else {
                $entity->setAllowordering("0");
            }
            //seteo entityImagen
            $em = $this->getDoctrine()->getManager();

            if (!is_null($idEntityImnage)) {
                $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
                if (!is_null($entityImage)) {
                    $entity->setImage($entityImage);
                }
            }
            $isDeleted = $request->request->get('folder_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }

            //seteo menu
            //seteo el menu
            $session = $request->getSession();
            $idNegocio = $session->get('idNegocio');
            $business = $em->getRepository('SistemaIRMBundle:Businesses')->find($idNegocio);
            $entity->setMenu($business->getMenu());

            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');


        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Displays a form to create a new Folders entity.
     *
     * @Route("/new", name="admin_folders_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new FoldersType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Finds and displays a Folders entity.
     *
     * @Route("/{id}", name="admin_folders_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Folders entity.
     *
     * @Route("/{id}/edit", name="admin_folders_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new FoldersType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        $formEntityImages = $this->crearFormulario(new EntityImagesType(), $entityImages, 'admin_entityimages_post_ajax', 'formEntityImages');


        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Edits an existing Folders entity.
     *
     * @Route("/{id}", name="admin_folders_update")
     * @Method("PUT")
     * @Template("SistemaIRMBundle:Folders:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new FoldersType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        if (!is_null($entity->getImage())) {
            $resgImagen = $entity->getImage()->getId();
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        $idEntityImnage = $request->request->get('imagenItem', null);
        if ($editForm->isValid()) {
            if (!is_null($idEntityImnage)) {
                $entityImage = $em->getRepository('SistemaIRMBundle:EntityImages')->find($idEntityImnage);
                if ((!is_null($entityImage)) && ($idEntityImnage != $resgImagen)) {
                    $entity->setImage($entityImage);
                }
            }
            if (is_null($entity->getParent())) {
                $root = $em->getRepository('SistemaIRMBundle:Folders')->getRootFolder();
                $entity->setParent($root);
            }
            $isDeleted = $request->request->get('folder_isDeleted', null);
            //seteo is deleted
            if (!is_null($isDeleted)) {
                $entity->setIsdeleted("1");
            } else {
                $entity->setIsdeleted("0");
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        //creo formulario entity image 
        $configEntityImages = $configs = Yaml::parse(file_get_contents($this->get('kernel')->getRootDir() . '/../src/Sistema/IRMBundle/Resources/config/EntityImages.yml'));

        $entityImages = new $configEntityImages['entity']();
        //$formEntityImages = $this->createCreateForm($configEntityImages, $entityImages);
        $formEntityImages = $this->createForm(new EntityImagesType(), $entityImages, array(
            'action' => $this->generateUrl($configEntityImages['create']),
            'method' => 'POST',
        ));

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formEntityImage' => $formEntityImages->createView(),
        );
    }

    /**
     * Deletes a Folders entity.
     *
     * @Route("/{id}", name="admin_folders_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Folders.
     *
     * @Route("/exporter/{format}", name="admin_folders_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/autocomplete-forms/get-menu", name="Folders_autocomplete_menu")
     */
    public function getAutocompleteMenus() {
        $options = array(
            'repository' => "SistemaIRMBundle:Menus",
            'field' => "name",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb
                ->select('a')
                ->from($options['repository'], 'a')
                ->leftJoin('a.businesses', 'b')
                ->Where("a." . $options['field'] . " LIKE ?1 and b.id= :bus")
                ->setParameter(1, "%" . $term . "%")
                ->setParameter('bus', $idNegocio)
                ->getQuery()
                ->getResult()
        ;

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/autocomplete-forms/get-image", name="Folders_autocomplete_image")
     */
    public function getAutocompleteEntityImages() {
        $options = array(
            'repository' => "SistemaIRMBundle:EntityImages",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/autocomplete-forms/get-products", name="Folders_autocomplete_products")
     */
    public function getAutocompleteProducts() {
        $options = array(
            'repository' => "SistemaIRMBundle:Products",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/autocomplete-forms/get-children", name="Folders_autocomplete_children")
     */
    public function getAutocompleteFolders() {
        $options = array(
            'repository' => "SistemaIRMBundle:Folders",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/autocomplete-forms/get-parent", name="Folders_autocomplete_parent")
     */
    public function getAutocompleteFoldersparent() {
        $options = array(
            'repository' => "SistemaIRMBundle:Folders",
            'field' => "name",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $session = $request->getSession();
        $idNegocio = $session->get('idNegocio');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entities = $qb
                ->select('a')
                ->from($options['repository'], 'a')
                ->leftJoin('a.menu', 'm')
                ->leftJoin('m.businesses', 'b')
                ->Where("a." . $options['field'] . " LIKE ?1 and b.id= :bus")
                ->setParameter(1, "%" . $term . "%")
                ->setParameter('bus', $idNegocio)
                ->getQuery()
                ->getResult()
        ;

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    private function crearFormulario($formType, $entity, $ruta, $id = "form") {
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($ruta),
            'method' => 'POST',
            'attr' => array('id' => $id)
        ));

        return $form;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/create-folder/", name="Folders_create_byAjax")
     */
    public function createFolderByAjax() {

        $request = $this->getRequest();
        $categoria = $request->request->get('jstree', null);
        //ladybug_dump($categoria);

        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $idMenu = $session->get('idMenu');
        $parent = $em->getRepository('SistemaIRMBundle:Folders')->find($categoria[0]["id"]); 
        //ladybug_dump_die($parent->getId());
        $menu = $em->getRepository('SistemaIRMBundle:Menus')->find($idMenu);
        
        $folder = new Folders();

        $folder->setName("Nueva Folder");
        $folder->setParent($parent);
        $folder->setMenu($menu);
        $uuid = Uuid::uuid1();
        $uuid = $uuid->getFieldsHex();
        $uuid = $uuid["time_low"] . "-" .
                $uuid["time_mid"] . "-" .
                $uuid["time_hi_and_version"] . "-" .
                $uuid["clock_seq_hi_and_reserved"] .
                $uuid["clock_seq_low"] . "-" .
                $uuid["node"];
        $folder->setUuid($uuid);
        $folder->setVisible(1);
        $folder->setAllowordering(0);
        $folder->setStatus(0);
        $folder->setIsdeleted(0);
        
        $em->persist($folder);
        $em->flush();

        $array[] = array(
            'resultado' => true,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/rename-folder/", name="Folders_rename_byAjax")
     */
    public function renameFolderByAjax() {

        $request = $this->getRequest();
        $categoria = $request->request->get('jstree', null);
        
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        
        $folder = $em->getRepository('SistemaIRMBundle:Folders')->find($categoria[0]["id"]);
        $folder->setName($categoria[0]["text"]);
        
        $em->persist($folder);
        $em->flush();

        $array[] = array(
            'resultado' => true,
        );


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
        
    }

    function recorrido_recursivo($tree, $parent_id = 0, $result = array(), $level = 0){
        foreach($tree as $child) {
            $result[$child["id"]] = array(
                'id' => $child["id"],
                'nombre' => $child["text"],
                'parent_id' => $parent_id,
                'level' => $level
            );

            if (!empty($child["children"])) {
                $result = $this->recorrido_recursivo($child["children"], $child["id"], $result, $level+1);
            }
        }
        return $result;     
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/move-folder/", name="Folders_move_byAjax")
     */
    public function moveFolderByAjax() {
        $request = $this->getRequest();
        $categoria = $request->request->get('jstree', true);
        ladybug_dump_die($categoria);
        $em = $this->getDoctrine()->getManager();
        $folder = $em->getRepository('SistemaIRMBundle:Folders')->find($categoria[0]["id"]);
        $parent = $em->getRepository('SistemaIRMBundle:Folders')->find($categoria[0]["parent"]);
        
        $folder->setParent($parent);
        $em->persist($folder);
        $em->flush();
        
        $array[] = array(
            'resultado' => true,
        );
        
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Folders entity.
     *
     * @Route("/deleted-folder/", name="Folders_deleted_byAjax")
     */
    public function deletedFolderByAjax() {
        $request = $this->getRequest();
        $categoria = $request->request->get('jstree', null);

        $em = $this->getDoctrine()->getManager();
        $folder = $em->getRepository('SistemaIRMBundle:Folders')->find($categoria[0]["id"]);
        
        $em->remove($folder);
        $em->flush();
        
        $array[] = array(
            'resultado' => true,
        );
        
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }
}
