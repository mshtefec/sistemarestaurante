<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * UserType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {//New
            $required = true;
        } else {//Edit
            $required = false;
        }
        $builder
            ->add('username', null, array(
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'form.username',
                )
            ))
            ->add('email', 'email', array(
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'form.email',
                )
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'required' => $required,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array(
                    'label' => 'form.password',
                    'attr' => array(
                        'class' => 'form-control',
                        // 'placeholder' => 'form.password',
                    )
                ),
                'second_options' => array(
                    'label' => 'form.password_confirmation',
                    'attr' => array(
                        'class' => 'form-control',
                        // 'placeholder' => 'form.password_confirmation',
                    )
                ),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('user_roles', 'select2', array(
                'label'              => 'user.form.roles',
                'translation_domain' => 'SistemaIRMBundle',
                'class'              => 'Sistema\UserBundle\Entity\Role',
                'url'                => 'User_autocomplete_user_roles',
                'configs'            => array(
                    'multiple' => true, //required true or false
                    'width'    => 'off',
                ),
                'label_attr'         => array(
                    'class'    => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                ),
                'attr'               => array(
                    'class'    => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                ),
            ))
            ->add('enabled', null, array(
                'label'              => 'user.form.enabled',
                'translation_domain' => 'SistemaIRMBundle',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_userbundle_user';
    }

}
