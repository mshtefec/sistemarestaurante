<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Sistema\IRMBundle\Filter\negocioInterface;
use Rhumsaa\Uuid\Uuid;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Sistema\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_roles")
     */
    protected $user_roles;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="accessLevel", type="integer", nullable=true)
     */
    private $accesslevel;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="creationTimeStamp", type="datetime", nullable=true)
     */
    private $creationtimestamp;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="timeStamp", type="datetime", nullable=false)
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=false)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="passwordProv", type="string", length=50, nullable=true)
     */
    private $passwordprov;

    /**
     * @var integer
     *
     * @ORM\Column(name="passwordProvExpiration", type="bigint", nullable=true)
     */
    private $passwordprovexpiration;

    /**
     * @var string
     *
     * @ORM\Column(name="secretQuestion", type="string", length=150, nullable=true)
     */
    private $secretquestion;

    /**
     * @var string
     *
     * @ORM\Column(name="secretAnswer", type="string", length=100, nullable=true)
     */
    private $secretanswer;

    /**
     * @var string
     *
     * @ORM\Column(name="firstNames", type="string", length=100, nullable=true)
     */
    private $firstnames;

    /**
     * @var string
     *
     * @ORM\Column(name="lastNames", type="string", length=100, nullable=true)
     */
    private $lastnames;

    /**
     * @var string
     *
     * @ORM\Column(name="alternateEmail", type="string", length=100, nullable=true)
     */
    private $alternateemail;

    /**
     * @var string
     *
     * @ORM\Column(name="countryCode", type="string", length=3, nullable=true)
     */
    private $countrycode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=200, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=3, nullable=false)
     */
    private $lang;

    /**
     * @var integer
     *
     * @ORM\Column(name="server_schema_id", type="integer", nullable=true)
     */
    private $serverSchemaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="syncProcessTimeStamp", type="bigint", nullable=false)
     */
    private $syncprocesstimestamp;

    /**
     * @var \Sistema\IRMBundle\Entity\User_businesses
     *
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\UserBusinesses", mappedBy="user",cascade={"all"})
     * */
    private $userBusinesses;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\EntityImages", mappedBy="user")
     * */
    private $entityImages;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Menus", mappedBy="user")
     * */
    private $menus;

    /**
     * @var string
     *
     * @ORM\Column(name="configuraciones", type="array")
     */
    private $configuraciones;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\IRMBundle\Entity\Orders", mappedBy="user")
     * */
    private $orders;

    public function __construct() {
        parent::__construct();
        $this->user_roles = new ArrayCollection();
        $this->userBusinesses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entityImages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->menus = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setLang(0);
        $this->setSyncprocesstimestamp(0);
        $this->uuid = $this->generoUUID();
    }

    private function generoUUID() {
        //genero uuid
        $uuid = Uuid::uuid1();
        $uuid = $uuid->getFieldsHex();
        $uuid = $uuid["time_low"] . "-" .
                $uuid["time_mid"] . "-" .
                $uuid["time_hi_and_version"] . "-" .
                $uuid["clock_seq_hi_and_reserved"] .
                $uuid["clock_seq_low"] . "-" .
                $uuid["node"];
        return $uuid;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns an ARRAY of Role objects with the default Role object appended.
     * @return array
     */
    public function getRoles() {
        $roles = $this->user_roles->toArray();
        array_push($roles, "ROLE_USER");
        return $roles;
    }

    /**
     * Returns the true ArrayCollection of Roles.
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getRolesCollection() {
        return $this->user_roles;
    }

    /**
     * Pass a string, get the desired Role object or null.
     * @param  string    $role
     * @return Role|null
     */
    public function getRole($role) {
        foreach ($this->getRoles() as $roleItem) {
            if ($role == $roleItem) {
                return $roleItem;
            }
        }

        return null;
    }

    /**
     * Pass a string, checks if we have that Role. Same functionality as getRole() except returns a real boolean.
     * @param  string  $role
     * @return boolean
     */
    public function hasRole($role) {
        if ($this->getRole($role)) {
            return true;
        }

        return false;
    }

    /**
     * Adds a Role OBJECT to the ArrayCollection. Can't type hint due to interface so throws Exception.
     * @throws Exception
     * @param  Role      $role
     */
    public function addRole($role) {
        if (!$role instanceof Role) {
            throw new \Exception("addRole takes a Role object as the parameter");
        }

        if (!$this->hasRole($role->getRole())) {
            $this->user_roles->add($role);
        }
    }

    /**
     * Pass a string, remove the Role object from collection.
     * @param string $role
     */
    public function removeRole($role) {
        $roleElement = $this->getRole($role);
        if ($roleElement) {
            $this->user_roles->removeElement($roleElement);
        }
    }

    /**
     * Pass an ARRAY of Role objects and will clear the collection and re-set it with new Roles.
     * Type hinted array due to interface.
     * @param array $user_roles Of Role objects.
     */
    public function setRoles(array $user_roles) {
        $this->user_roles->clear();
        foreach ($user_roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * Directly set the ArrayCollection of Roles. Type hinted as Collection which is the parent of (Array|Persistent)Collection.
     * @param Doctrine\Common\Collections\Collection $role
     */
    public function setRolesCollection(Collection $collection) {
        $this->user_roles = $collection;
    }

    /**
     * Add user_roles
     *
     * @param  \Sistema\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Sistema\UserBundle\Entity\Role $userRoles) {
        $this->user_roles[] = $userRoles;

        return $this;
    }

    /**
     * Remove user_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Sistema\UserBundle\Entity\Role $userRoles) {
        $this->user_roles->removeElement($userRoles);
    }

    /**
     * Get user_roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles() {
        return $this->user_roles;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return User
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set accesslevel
     *
     * @param integer $accesslevel
     * @return User
     */
    public function setAccesslevel($accesslevel) {
        $this->accesslevel = $accesslevel;

        return $this;
    }

    /**
     * Get accesslevel
     *
     * @return integer 
     */
    public function getAccesslevel() {
        return $this->accesslevel;
    }

    /**
     * Set creationtimestamp
     *
     * @param \DateTime $creationtimestamp
     * @return User
     */
    public function setCreationtimestamp($creationtimestamp) {
        $this->creationtimestamp = $creationtimestamp;

        return $this;
    }

    /**
     * Get creationtimestamp
     *
     * @return \DateTime 
     */
    public function getCreationtimestamp() {
        return $this->creationtimestamp;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return User
     */
    public function setTimestamp($timestamp) {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp() {
        return $this->timestamp;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return User
     */
    public function setUuid($uuid) {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set passwordprov
     *
     * @param string $passwordprov
     * @return User
     */
    public function setPasswordprov($passwordprov) {
        $this->passwordprov = $passwordprov;

        return $this;
    }

    /**
     * Get passwordprov
     *
     * @return string 
     */
    public function getPasswordprov() {
        return $this->passwordprov;
    }

    /**
     * Set passwordprovexpiration
     *
     * @param integer $passwordprovexpiration
     * @return User
     */
    public function setPasswordprovexpiration($passwordprovexpiration) {
        $this->passwordprovexpiration = $passwordprovexpiration;

        return $this;
    }

    /**
     * Get passwordprovexpiration
     *
     * @return integer 
     */
    public function getPasswordprovexpiration() {
        return $this->passwordprovexpiration;
    }

    /**
     * Set secretquestion
     *
     * @param string $secretquestion
     * @return User
     */
    public function setSecretquestion($secretquestion) {
        $this->secretquestion = $secretquestion;

        return $this;
    }

    /**
     * Get secretquestion
     *
     * @return string 
     */
    public function getSecretquestion() {
        return $this->secretquestion;
    }

    /**
     * Set secretanswer
     *
     * @param string $secretanswer
     * @return User
     */
    public function setSecretanswer($secretanswer) {
        $this->secretanswer = $secretanswer;

        return $this;
    }

    /**
     * Get secretanswer
     *
     * @return string 
     */
    public function getSecretanswer() {
        return $this->secretanswer;
    }

    /**
     * Set firstnames
     *
     * @param string $firstnames
     * @return User
     */
    public function setFirstnames($firstnames) {
        $this->firstnames = $firstnames;

        return $this;
    }

    /**
     * Get firstnames
     *
     * @return string 
     */
    public function getFirstnames() {
        return $this->firstnames;
    }

    /**
     * Set lastnames
     *
     * @param string $lastnames
     * @return User
     */
    public function setLastnames($lastnames) {
        $this->lastnames = $lastnames;

        return $this;
    }

    /**
     * Get lastnames
     *
     * @return string 
     */
    public function getLastnames() {
        return $this->lastnames;
    }

    /**
     * Set alternateemail
     *
     * @param string $alternateemail
     * @return User
     */
    public function setAlternateemail($alternateemail) {
        $this->alternateemail = $alternateemail;

        return $this;
    }

    /**
     * Get alternateemail
     *
     * @return string 
     */
    public function getAlternateemail() {
        return $this->alternateemail;
    }

    /**
     * Set countrycode
     *
     * @param string $countrycode
     * @return User
     */
    public function setCountrycode($countrycode) {
        $this->countrycode = $countrycode;

        return $this;
    }

    /**
     * Get countrycode
     *
     * @return string 
     */
    public function getCountrycode() {
        return $this->countrycode;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return User
     */
    public function setLang($lang) {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang
     *
     * @return string 
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * Set serverSchemaId
     *
     * @param integer $serverSchemaId
     * @return User
     */
    public function setServerSchemaId($serverSchemaId) {
        $this->serverSchemaId = $serverSchemaId;

        return $this;
    }

    /**
     * Get serverSchemaId
     *
     * @return integer 
     */
    public function getServerSchemaId() {
        return $this->serverSchemaId;
    }

    /**
     * Set syncprocesstimestamp
     *
     * @param integer $syncprocesstimestamp
     * @return User
     */
    public function setSyncprocesstimestamp($syncprocesstimestamp) {
        $this->syncprocesstimestamp = $syncprocesstimestamp;

        return $this;
    }

    /**
     * Get syncprocesstimestamp
     *
     * @return integer 
     */
    public function getSyncprocesstimestamp() {
        return $this->syncprocesstimestamp;
    }

    /**
     * Add entityImages
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $entityImages
     * @return User
     */
    public function addEntityImage(\Sistema\IRMBundle\Entity\EntityImages $entityImages) {
        $this->entityImages[] = $entityImages;

        return $this;
    }

    /**
     * Remove entityImages
     *
     * @param \Sistema\IRMBundle\Entity\EntityImages $entityImages
     */
    public function removeEntityImage(\Sistema\IRMBundle\Entity\EntityImages $entityImages) {
        $this->entityImages->removeElement($entityImages);
    }

    /**
     * Get entityImages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntityImages() {
        return $this->entityImages;
    }

    /**
     * Add menus
     *
     * @param \Sistema\IRMBundle\Entity\Menus $menus
     * @return User
     */
    public function addMenu(\Sistema\IRMBundle\Entity\Menus $menus) {
        $this->menus[] = $menus;

        return $this;
    }

    /**
     * Remove menus
     *
     * @param \Sistema\IRMBundle\Entity\Menus $menus
     */
    public function removeMenu(\Sistema\IRMBundle\Entity\Menus $menus) {
        $this->menus->removeElement($menus);
    }

    /**
     * Get menus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMenus() {
        return $this->menus;
    }

    /**
     * Adds/sets an element in the collection at the index / with the specified key.
     *
     * When the collection is a Map this is like put(key,value)/add(key,value).
     * When the collection is a List this is like add(position,value).
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setItemConfiguraciones($key, $value) {
        $this->configuraciones[$key] = $value;
    }

    /**
     * Set configuraciones
     *
     * @param array $configuraciones
     * @return Configuracion
     */
    public function setConfiguraciones($configuraciones) {
        $this->configuraciones = $configuraciones;

        return $this;
    }

    /**
     * Get configuraciones
     *
     * @return array 
     */
    public function getConfiguraciones() {
        return $this->configuraciones;
    }

    /**
     * Add userBusinesses
     *
     * @param \Sistema\IRMBundle\Entity\User_businesses $userBusinesses
     * @return User
     */
    public function addUserBusiness(\Sistema\IRMBundle\Entity\UserBusinesses $userBusinesses) {
        $userBusinesses->setUser($this);
        $this->userBusinesses[] = $userBusinesses;

        return $this;
    }

    /**
     * Remove userBusinesses
     *
     * @param \Sistema\IRMBundle\Entity\User_businesses $userBusinesses
     */
    public function removeUserBusiness(\Sistema\IRMBundle\Entity\UserBusinesses $userBusinesses) {
        $this->userBusinesses->removeElement($userBusinesses);
    }

    /**
     * Get userBusinesses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserBusinesses() {
        return $this->userBusinesses;
    }

    /**
     * Add menus
     *
     * @param \Sistema\IRMBundle\Entity\Orders $orders
     * @return User
     */
    public function addOrder(\Sistema\IRMBundle\Entity\Orders $orders) {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove menus
     *
     * @param \Sistema\IRMBundle\Entity\Orders $orders
     */
    public function removeOrder(\Sistema\IRMBundle\Entity\Orders $orders) {
        $this->orders->removeElement($orders);
    }

    /**
     * Get menus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders() {
        return $this->orders;
    }

}
