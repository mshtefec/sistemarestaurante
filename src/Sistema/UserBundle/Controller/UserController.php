<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\User;
use Sistema\UserBundle\Form\UserType;
use Sistema\UserBundle\Form\UserFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * User controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/superadmin/user")
 */
class UserController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/User.yml',
    );

    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new UserFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="admin_user_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:User:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new UserType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // //genero uuid
            // $uuid = Uuid::uuid1();
            // $uuid = $uuid->getFieldsHex();
            // $uuid = $uuid["time_low"] . "-" .
            //         $uuid["time_mid"] . "-" .
            //         $uuid["time_hi_and_version"] . "-" .
            //         $uuid["clock_seq_hi_and_reserved"] .
            //         $uuid["clock_seq_low"] . "-" .
            //         $uuid["node"];
            // $entity->setUuid($uuid);
            $this->setSecurePassword($entity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new UserType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="admin_user_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new UserType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="admin_user_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:User:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new UserType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $current_pass = $entity->getPassword(); //pass anterior
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //si pass es nulo guardo al anterior
            if (is_null($entity->getPassword())) {
                $entity->setPassword($current_pass);
            } elseif ($current_pass != $entity->getPassword()) {//si el password cambio lo codifico
                $this->setSecurePassword($entity);
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="admin_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-user_roles", name="User_autocomplete_user_roles")
     */
    public function getAutocompleteRole() {
        $options = array(
            'repository' => "SistemaUserBundle:Role",
            'field' => "role_name",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Exporter User.
     *
     * @Route("/exporter/{format}", name="admin_user_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    private function setSecurePassword($entity) {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-user", name="User_autocomplete_user_byBussineses")
     */
    public function getUsuarios() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $session = $request->getSession();
        $negocio = $session->get('asignarNegocioaUsuario');
        $em = $this->getDoctrine()->getManager();
       
        $entities = $em->getRepository('SistemaIRMBundle:Businesses')->getAllUserNotInBusinesses($negocio,$this->getUser()->getId(), $term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
