<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * UserInfoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UserInfoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {//New
            $required = true;
        } else {//Edit
            $required = false;
        }
        $builder
            ->add('alternateemail', null, array(
                'label' => 'Alternate Email',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('firstnames', null, array(
                'label' => 'First Name',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('lastnames', null, array(
                'label' => 'Last Name',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('address', null, array(
                'label' => 'Address',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('country', null, array(
                'label' => 'Country',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('countrycode', null, array(
                'label' => 'Country Code',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('phone', null, array(
                'label' => 'Phone',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_userbundle_userinfo';
    }

}
