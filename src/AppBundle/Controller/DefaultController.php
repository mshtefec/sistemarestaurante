<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\IRMBundle\Entity\Folders;
use Sistema\IRMBundle\Entity\Products;
use Sistema\IRMBundle\Entity\Menus;
use Sistema\IRMBundle\Entity\Orders;
use Sistema\IRMBundle\Entity\OrderItems;
use Sistema\IRMBundle\Entity\EntityImages;
use Sistema\IRMBundle\Entity\Tables;
use Symfony\Component\Yaml\Yaml;
use Hateoas\HateoasBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Rhumsaa\Uuid\Uuid;

class DefaultController extends Controller
{
    /**
     * Lists all Folders entities.
     *
     * @Route("/", name="homepage")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        
        $em = $this->getDoctrine()->getManager();

        $business = $em->getRepository('SistemaIRMBundle:Businesses')->findAll();

        return $this->render('AppBundle:Default:index.html.twig', array(
            'businesses' => $business,
        ));
    }

    /**
     * Finds and displays a Menus entity.
     *
     * @Route("/negocio/{idNeg}", name="menu_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($idNeg) {
        
        $em = $this->getDoctrine()->getManager();

        $business = $em->getRepository('SistemaIRMBundle:Businesses')->find($idNeg);

        if ($business->getMenu()) {
          $idMenu = $business->getMenu()->getId();
          $menu = $em->getRepository('SistemaIRMBundle:Menus')->find($idMenu);
          $categories = $em->getRepository('SistemaIRMBundle:Folders')->getFolderByMenu($idMenu);
        }

        $query = $em->createQuery(
            'SELECT p, m FROM SistemaIRMBundle:Products p
            JOIN p.menu m
            WHERE m.id = :id'
        )->setParameter('id', $idMenu);

        $products = $query->getResult();

        return $this->render('AppBundle:Default:show.html.twig', array(
            'categories' => $categories,
            'products' => $products,
            'menu' => $menu,
            'business' => $business,
        ));
    }

    /**
     * Finds and displays a Menus entity.
     *
     * @Route("/traerProductos/{id}", name="traer_productos", options={"expose"=true})
     * @Method("GET")
     */
    public function traerProductosAction(Request $request, $id) {
        $data = $request->request->all();
        
        $session = $this->getRequest()->getSession();

        $em = $this->getDoctrine()->getManager();
        
        $ids = array();
        array_push($ids, $id);
        
        if ($id != 0) {
            $selectedCategory = $em->getRepository('SistemaIRMBundle:Folders')->find($id);
            $childrens = $selectedCategory->getChildren()->getValues();    
            foreach ($childrens as $key => $child) {
                array_push($ids, $child->getId());
            }
            $products = $em->getRepository('SistemaIRMBundle:Products')->getProductsByCategories($ids);
        } else {
            $products = $em->getRepository('SistemaIRMBundle:Products')->getProductsByMenu($session->get('idMenu'));
        }   

        return $this->render('AppBundle:Default:products.html.twig', array(
            'products' => $products,
        ));
        
    }

    /**
     * Finds and displays a Menus entity.
     *
     * @Route("user/carrito", name="menu_carrito")
     * @Method("GET")
     * @Template()
     */
    public function carritoAction(Request $request) {
         
        $session = $request->getSession();
        $sessionPrices = $session->get('prices');
        $em = $this->getDoctrine()->getManager();

        return $this->render('AppBundle:Default:carrito.html.twig', array(
            'prices' => $sessionPrices,
        ));
    }

    /**
     * Finds and displays a Menus entity.
     *
     * @Route("user/orders", name="user_orders")
     * @Method("GET")
     * @Template()
     */
    public function ordersAction(Request $request) {
        $userId = $this->get('security.context')->getToken()->getUser()->getId();
        //ladybug_dump_die($userId);

        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('SistemaIRMBundle:Orders')->getOrdersByUserId($userId);

        return $this->render('AppBundle:Default:orders.html.twig', array(
            'orders' => $orders,
        ));   
    }

    /**
     * set Session Carrito.
     *
     * @Route("/sessionSet/{prices}", name="set_session", options={"expose"=true})
     * @Method("GET")
     */
    public function setSessionAction(Request $request, $prices) {
        
        //desconcateno el string de precio o precios que viene cada vez que agrego producto
        //marcados por lo/s checkbox
        $precios = explode(",", $prices);
        $realPrices = array();
        $realPricesMod = array();
        foreach ($precios as $precio) {
            //desconcateno prices
            if (strstr($precio, "-.-")) {
                $price = explode("-.-", $precio);   
                $realPrices[(int)$price[0]] = array(
                    'id' => $price[0],
                    'name' => $price[1],
                    'price' => $price[2],
                    'product' => $price[3]
                );
            } else {
            //desconcateno pricesModifiers    
                $priceMod = explode("^", $precio);   
                $realPricesMod[(int)$priceMod[0]] = array(
                    'id' => $priceMod[0],
                    'name' => $priceMod[1],
                    'price' => $priceMod[2],
                    'product' => $priceMod[3]
                );
            }
        }
        
        $newPrices = array();
        foreach ($realPrices as $key => $realPrice) {
          
          foreach ($newPrices as $keyP => $p){
            if ($p['producto'] == $realPrice['product']){
              array_push($newPrices[$keyP]['nombre'], $realPrice['name']);
              array_push($newPrices[$keyP]['precio'], $realPrice['price']);
            } else {
              unset($newPrices[$keyP+1]); 
            }
             
          }

          $newPrices[] = array(
              'producto'      => $realPrice['product'],
              'nombre'        => array($realPrice['name']),
              'precio'        => array($realPrice['price']),
              'modificadores' => array(
                    'modifNombre' => array(),
                    'modifPrecio' => array(),
              ),
          );
        }

        foreach ($realPricesMod as $key => $priceMod) {
          foreach ($newPrices as $keyP => $newPrice) {
            if ($priceMod['product'] == $newPrice['producto']) {
              array_push($newPrices[$keyP]['modificadores']['modifNombre'], $priceMod['name']);
              array_push($newPrices[$keyP]['modificadores']['modifPrecio'], $priceMod['price']);
            }
          }  
        }

        //ladybug_dump_die($sessionPrices);

        $session = $request->getSession();
        //si session ya tenia precios guardados solo guardo los nuevos precios ingresados
        if ($session->has('prices')) {
            $sessionPrices = $session->get('prices');
            foreach ($newPrices as $key => $newPrice) {
              $sessionPrices[$key] = $newPrice; 
            }
            $session->set('prices', $sessionPrices);
        //si no seteo los precios ingresado por primera vez    
        } else {
            $session->set('prices', $newPrices);   
        }

        //idem pero con priceModifiers
        /*
        if ($session->has('pricesModifiers')) {
            $sessionPricesModifiers = $session->get('pricesModifiers');
            foreach ($realPricesMod as $key => $realPriceMod) {
                $sessionPricesModifiers[$key] = $realPriceMod; 
            }
            $session->set('pricesModifiers', $sessionPricesModifiers);
        } else {
            $session->set('pricesModifiers', $realPricesMod);   
        }
        */

        $sessionPrices = $session->get('prices');
        //ladybug_dump_die($sessionPrices);
        //$sessionPricesMod = $session->get('pricesModifiers');

        $em = $this->getDoctrine()->getManager();
        $deliveryDates = $em->getRepository('SistemaIRMBundle:DeliveryDates')->findAll();
        $tables = $em->getRepository('SistemaIRMBundle:Tables')->findAll();
        
        return $this->render('AppBundle:Default:carrito.html.twig', array(
            'prices' => $sessionPrices,
            //'pricesModifiers' => $sessionPricesMod,
            'deliveryDates' => $deliveryDates,
            'tables' => $tables,
        ));
    }

    /**
     * deleted Session Carrito.
     *
     * @Route("/sessionDeleted/{id}", name="deleted_session", options={"expose"=true})
     * @Method("GET")
     */
    public function deletedSessionAction(Request $request, $id) {
        
        $session = $request->getSession();
        $sessionPrices = $session->get('prices');
        foreach ($sessionPrices as $key => $price) {
            if ($key == $id) {
                unset($sessionPrices[$key]);
            } 
        }
        $session->set('prices', $sessionPrices);
        
        $response = new JsonResponse();
        return $response;
    }

    /**
     * deleted Session Carrito.
     *
     * @Route("/modSessionDeleted/{id}", name="deleted_session_mod", options={"expose"=true})
     * @Method("GET")
     */
    public function deletedSessionModAction(Request $request, $id) {
        
        $session = $request->getSession();
        $sessionPricesMod = $session->get('pricesModifiers');
        foreach ($sessionPricesMod as $key => $priceMod) {
            if ($key == $id) {
                unset($sessionPricesMod[$key]);
            } 
        }
        $session->set('pricesModifiers', $sessionPricesMod);
        
        $response = new JsonResponse();
        return $response;
    }

    /**
     * generar orden Carrito.
     *
     * @Route("/generarOrden/{amount}/{items}/{subTotals}/{fechaEntrega}/{destino}/{nota}", name="generar_orden", options={"expose"=true})
     * @Method("GET")
     */
    public function generarOrdenAction(Request $request, $amount, $items, $subTotals, $fechaEntrega, $destino, $nota) {
         
        $session = $request->getSession();
        $bussinesId = $session->get('idNegocio');
        $user = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $business = $em->getRepository('SistemaIRMBundle:Businesses')->find($bussinesId);
        
        //ladybug_dump_die($fechaEntrega);
        
        $orden = new Orders();
        //genero uuid
        $uuid = Uuid::uuid1();
        $uuid = $uuid->getFieldsHex();
        $uuid = $uuid["time_low"] . "-" .
                $uuid["time_mid"] . "-" .
                $uuid["time_hi_and_version"] . "-" .
                $uuid["clock_seq_hi_and_reserved"] .
                $uuid["clock_seq_low"] . "-" .
                $uuid["node"];
        $orden->setUuid($uuid);
        $orden->setUser($user);
        $orden->setAmount($amount);
        $orden->setIsDeleted(0);
        $orden->setBusiness($business);
        $orden->setDeliverydate($fechaEntrega);
        $orden->setDeviceuuid(null);
        $orden->setNote($nota);
        
        //si no existe un table creado, seteo userAddress
        //y table en null
        if ($destino == 'No Disponible') {
          $destino = $user->getAddress();
          $orden->setUserAddress($destino);
          $orden->setTable(null);  
        //si existe busco el table seleccionado, seteo table
        //y userAddress en null 
        } else {
          $table = $em->getRepository('SistemaIRMBundle:Tables')->findByName($destino);
          $orden->setUserAddress(null);
          $orden->setTable($table[0]);
        }
        
        $orden->setTaxName(null);
        $orden->setTaxValue(null);
        $orden->setGuestsQuantity(null);

        $orders = $this->parseItems($items, $subTotals, $orden);
        $contItems = 0;
        foreach ($orders as $key => $order) {
          if ($key != 0) {
            $orden->addOrderItem($order);
            $contItems = $contItems + 1;
          }
        }
        $orden->setGuestsquantity($contItems);

        $em->persist($orden);        
        $em->flush();
        
        $response = new JsonResponse();
        return $response;
    }

    public function parseItems($pricesId, $subPrices, $order) {
        
        $em = $this->getDoctrine()->getManager();

        $items = array();

        $prices = array_map('intval', explode("-.-", $pricesId));
        $subTotal = array_map('floatval', explode("¬", $subPrices));
        
        //ladybug_dump_die($prices);

        foreach ($prices as $key => $price) {
          $product = $em->getRepository('SistemaIRMBundle:Products')->find($prices[$key]);
          
          $orderItem = new OrderItems();

          //genero uuid
          $uuid = Uuid::uuid1();
          $uuid = $uuid->getFieldsHex();
          $uuid = $uuid["time_low"] . "-" .
                  $uuid["time_mid"] . "-" .
                  $uuid["time_hi_and_version"] . "-" .
                  $uuid["clock_seq_hi_and_reserved"] .
                  $uuid["clock_seq_low"] . "-" .
                  $uuid["node"];
          $orderItem->setUuid($uuid);
          $orderItem->setProduct($product);
          $orderItem->setAmount($subTotal[$key]);
          $orderItem->setIsDeleted(0);
          $orderItem->setOrder($order);

          $em->persist($orderItem);
          
          array_push($items, $orderItem);
        }

        return $items;
    }
}
