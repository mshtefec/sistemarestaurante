<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\User;
use AppBundle\Form\UserInfoType;
use AppBundle\Form\UserInfoFilterType;

/**
 * UserInfo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("user/info")
 */
class UserInfoController extends Controller
{
	/**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'AppBundle/Resources/config/UserInfo.yml',
    );

    /**
     * Lists User entity.
     *
     * @Route("/", name="user_info")
     * @Method("GET")
     * @Template("AppBundle:UserInfo:index.html.twig")
     */
    public function indexAction() {
        return array(
            'entity' => $this->getUser(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/edit", name="user_info_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id = null) {
        $this->config['editType'] = new UserInfoType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($this->getUser()->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a entity.
    * @param array $config
    * @param $entity The entity
    * @return \Symfony\Component\Form\Form The form
    */
    protected function createEditForm($config, $entity)
    {
        $form = $this->createForm($config['editType'], $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array(
                    'class' => 'form-control btn-success',
                    'col'   => 'col-lg-2',
                )
                )
            )
        ;

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/", name="user_info_update")
     * @Method("PUT")
     * @Template("AppBundle:UserInfo:edit.html.twig")
     */
    public function updateAction($id = null) {
        $this->config['editType'] = new UserInfoType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        //$entity = $em->getRepository($config['repository'])->find($this->getUser()->getId());
        $entity = $this->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $this->generateUrl('user_info');
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }
}