irm
===

A Symfony project created on March 16, 2015, 6:49 pm.

Para setear Uuid en sistemas de 32 bits, editar la función calculateUuidTime
de src/uuid.php del bundle ramsey dentro de vendor ya que el rango de float
seteado dentro de la función no corresponde a sistemas de 32 bits

$uuidTime->add($sec)
         ->add($usec)
         ->add('122192928000000000'); //rango no permitido

$uuidTime->add($sec)
         ->add($usec)
         ->add('2147483647'); //rango permitido 

según la Moontoast Math Library.
On 64-bit systems, this number is 9223372036854775807. On 32-bit systems, it is 2147483647.

y setear la siguiente variable a true
 
public static $force32Bit = false;

ROLE_SUPERADMIN
ROLE_ADMIN
ROLE_NEGOCIO